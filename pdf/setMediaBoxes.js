/**
 * Called with two arguments:
 * mutool run setMediaboxes.js file_in file_out
 * 
 * Generate a front and back cover, based on the cover generated for print.
 * 
 * Copy over the single page from the print cover twice and set the page boxes
 * to reveal only the right (front) and left (back) side.
 */

 function mm (val) {
  return val / 0.352777778
}

function copyPage(dstDoc, srcDoc, pageNumber, dstFromSrc) {
  var srcPage, dstPage
  srcPage = srcDoc.findPage(pageNumber)
  dstPage = dstDoc.newDictionary()
  dstPage.Type = dstDoc.newName("Page")
  if (srcPage.MediaBox) dstPage.MediaBox = dstFromSrc.graftObject(srcPage.MediaBox)
  if (srcPage.Rotate) dstPage.Rotate = dstFromSrc.graftObject(srcPage.Rotate)
  if (srcPage.Resources) dstPage.Resources = dstFromSrc.graftObject(srcPage.Resources)
  if (srcPage.Contents) dstPage.Contents = dstFromSrc.graftObject(srcPage.Contents)
  dstDoc.insertPage(-1, dstDoc.addObject(dstPage))
}

var pageWidth = mm(289),
    pageHeight = mm(420),
    doc = new PDFDocument(argv[1]),
    dstDoc = new PDFDocument(),
    dstFromSrc = dstDoc.newGraftMap();

/**
 * Copy over the single page from the original cover to the new file
 */
var i, pageCount = doc.countPages();

for (i = 0; i < pageCount; ++i) {
  copyPage(dstDoc, doc, i, dstFromSrc);
  var pageLeft = dstDoc.findPage(dstDoc.countPages()-1);
  pageLeft.MediaBox = pageLeft.CropBox = pageLeft.TrimBox = pageLeft.BleedBox = [
      0,                      // Left X
      0,                      // Bottom Y
      pageWidth,              // Right X
      pageHeight              // Top Y
  ];
  copyPage(dstDoc, doc, i, dstFromSrc);
  var pageRight = dstDoc.findPage(dstDoc.countPages()-1);
  pageRight.MediaBox = pageRight.CropBox = pageRight.TrimBox = pageRight.BleedBox = [
      pageWidth,              // Left X
      0,                      // Bottom Y
      pageWidth * 2,          // Right X
      pageHeight              // Top Y
  ];
}

dstDoc.save(argv[2]);