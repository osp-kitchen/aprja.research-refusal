# Collect all spreads in one file, take only the first page
pdftk \
A=1.pdf \
B=2.pdf \
C=3.pdf \
D=4.pdf \
E=5.pdf \
F=6.pdf \
G=7.pdf \
H=8.pdf \
I=9.pdf \
J=10.pdf \
K=11.pdf \
L=12.pdf \
cat \
A1 B1 C1 D1 E1 F1 G1 H1 I1 J1 K1 L1 \
output spreads-concatenated.pdf;

# Double pages and set correct media boxes
mutool run setMediaBoxes.js spreads-concatenated.pdf spreads-raw.pdf;

# Clean pdf, set to black and white and, if necessary sample down images.
gs \
  -o "spreads-resampled.pdf" \
  -sDEVICE=pdfwrite \
  -sProcessColorModel=DeviceGray \
  -sColorConversionStrategy=Gray \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=300 \
  -dGrayImageResolution=300 \
  -dMonoImageResolution=300 \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
  -dAutoRotatePages=/None \
  "spreads-raw.pdf"

# Move backcover to the back, also reinsert page 7, emojii break in ghostscript somehow
pdftk A=spreads-resampled.pdf B=spreads-raw.pdf cat A2-7 B8 A9-end A1 output spreads.pdf;

# pdftk A=spreads.pdf B=spreads-raw.pdf cat A1-6 B8 A8-end output spreads-better.pdf;

# Clean up of intermediary files
rm spreads-concatenated.pdf spreads-raw.pdf spreads-resampled.pdf;

