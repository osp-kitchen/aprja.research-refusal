from flask import Flask, jsonify, send_file, request, abort
import os, os.path

spreads = [
  (0, 'Cover', (1, 24)),
  (10, '1 Inside cover', (2, 3)),
  (1, '2 Delink', (4, 5)),
  (2, '3 MELT', (6, 7)),
  (3, '4 BBL', (8, 9)),
  (4, '5 Bucles', (10, 11)),
  (5, '6 Interfaces of refusal', (12, 13)),
  (6, '7 logistical borders', (14, 15)),
  (7, '8 Nothing Happening here', (16, 17)),
  (8, '9 Not working group', (18, 19)),
  (9, '10 SIZ', (20, 21)),
  (11, '11 ?', (22, 23)),
]

spread_ids = list(map(lambda s: s[0], spreads))

SPREADS_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'spreads')

app = Flask(__name__)

@app.after_request
def add_headers(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers',
                        'Content-Type,Authorization')

  return response

# GET spreads
# returns a list of available spreads
@app.route('/api/spreads')
def list_spreads ():
  # [ (int:id, str:name, (int:pageNum, ...)), ... ]
  return jsonify(spreads)

# GET spread :spread_id
# Returns a list of versions for the given spread
@app.route('/api/spread/<int:spread_id>', methods=["GET"])
def list_versions (spread_id):
  if spread_id in spread_ids:
    versions = []
    # [ (int:id, int:timestamp), ... ]

    for path in os.listdir(os.path.join(SPREADS_FOLDER, str(spread_id))):
      if path.lower().endswith('.xml'):
        versions.append((int(path[:-4]), round(os.stat(os.path.join(SPREADS_FOLDER, str(spread_id), path)).st_ctime)))

    ## Sort based on creation time. Return list
    return jsonify(sorted(versions, key=lambda r: r[1], reverse=True)[:50])
  else:
    abort(404)

#GET spread :spread_id :version
@app.route('/api/spread/<int:spread_id>/<int:version>')
def get_version(spread_id, version):
  if spread_id in spread_ids:
    path = os.path.join(SPREADS_FOLDER, str(spread_id), str(version) + '.xml')

    if os.path.exists(path):
      return send_file(path, 'aplication/xml')
  abort(404)

# POST store :spread_id :blockly_xml
# Stores a version for the given spread
@app.route('/api/store', methods=["POST"])
def store ():
  spread_id = int(request.form['spread_id'])
  version_xml = request.form['version']

  if spread_id in spread_ids:
    next_key = len(list(filter(lambda p: p.endswith('.xml'), os.listdir(os.path.join(SPREADS_FOLDER, str(spread_id))))))
    path = os.path.join(SPREADS_FOLDER, str(spread_id), str(next_key) + '.xml')

    with open(path, 'w') as h:
      h.write(version_xml)

    return jsonify({'name': next_key })
  abort(404)