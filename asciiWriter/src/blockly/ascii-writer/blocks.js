import * as Blockly from 'blockly';

Blockly.Blocks['ascii_writer_drawing'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Drawing");
        this.appendStatementInput("commands")
            .setCheck(null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_apply_pattern'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Apply pattern");
        this.appendValueInput("layer")
            .setCheck(null)
            .appendField("Layer");
        this.appendValueInput("pattern")
            .setCheck(null)
            .appendField("Pattern");
        this.appendValueInput("marker")
            .setCheck(null)
            .appendField("Marker");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_layer'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make layer");
        this.appendValueInput("width")
            .setCheck(null)
            .appendField("Width");
        this.appendValueInput("height")
            .setCheck(null)
            .appendField("Height");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_pattern_sinus_vertical'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Vertical sinus");
        this.appendValueInput("period")
            .setCheck(null)
            .appendField("period");
        this.appendValueInput("amplitude")
            .setCheck(null)
            .appendField("amplitude");
        this.appendValueInput("offset_t")
            .setCheck(null)
            .appendField("offset T");
        this.appendValueInput("offset")
            .setCheck(null)
            .appendField("offset");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_pattern_cosinus_vertical'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Vertical sinus");
        this.appendValueInput("period")
            .setCheck(null)
            .appendField("period");
        this.appendValueInput("amplitude")
            .setCheck(null)
            .appendField("amplitude");
        this.appendValueInput("offset_t")
            .setCheck(null)
            .appendField("offset T");
        this.appendValueInput("offset")
            .setCheck(null)
            .appendField("offset");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_marker_text'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Marker running text");
        this.appendValueInput("text")
            .setCheck(null);
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_marker_repeating_text'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Marker repeating text");
        this.appendValueInput("text")
            .setCheck(null);
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_show_layer'] = {
    init: function () {
        this.appendValueInput("layer")
            .setCheck(null)
            .appendField("Display layer");
        this.setPreviousStatement(true, null);
        this.setNextStatement(false, null);
        this.setColour(270);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_merge_layer'] = {
    init: function () {
        this.appendValueInput("layers")
            .setCheck("Array")
            .appendField("Merge layers");
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_text_effects_space'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Insert char");
        this.appendValueInput("space")
            .setCheck("String");
        this.appendDummyInput()
            .appendField("between every character in text");
        this.appendValueInput("text")
            .setCheck("String");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_text_effects_double'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Double characters in ");
        this.appendValueInput("text")
            .setCheck("String");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_text_effects_uppercase'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Transform to uppercase ");
        this.appendValueInput("text")
            .setCheck("String");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


Blockly.Blocks['ascii_writer_text_effects_lowercase'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Transform to lowercase ");
        this.appendValueInput("text")
            .setCheck("String");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_text_column'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Set text in column");
        this.appendValueInput("column_width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Column width");
        this.appendValueInput("text")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Text");
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_geometry_offset'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Move layer");
        this.appendValueInput("x")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Horizontally");
        this.appendValueInput("y")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Vertically");
        this.appendValueInput("layer")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("layer");
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_text_column'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Set text in column");
        this.appendValueInput("column_width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Column width");
        this.appendValueInput("text")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Text");
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_first_then'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("At first");
        this.appendValueInput("first");
        this.appendDummyInput()
            .appendField("then");
        this.appendValueInput("then");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_create_geometries'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Create geometries");
        this.appendValueInput("inset")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Inset");
        this.appendValueInput("shorten")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Shorten");
        this.appendValueInput("prefix")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Prefix");
        this.appendValueInput("postfix")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Postfix");    
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "avoid_column_break_inside")
            .appendField("Avoid break inside");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("FALSE"), "avoid_column_break_after")
            .appendField("Avoid break after");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_flatten_texts'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Flatten texts");
        this.appendValueInput("fragments")
            .setCheck(null);
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_get_markdown'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Get markdown")
            .appendField(new Blockly.FieldTextInput("path"), "path");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_flow_markdown'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Flow text");
        this.appendValueInput("markdown")
            .setCheck(null)
            .appendField("Markdown");
        this.appendValueInput("filters")
            .setCheck("Array")
            .appendField("Filters");
        this.appendValueInput("frame")
            .setCheck(null)
            .appendField("Frame");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(270);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_render_markdown'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Render Markdown");
        this.appendValueInput("markdown")
            .setCheck(null)
            .appendField("Markdown");
        this.appendValueInput("filters")
            .setCheck("Array")
            .appendField("Filters");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

// Blockly.Blocks['ascii_writer_make_inline_filter'] = {
//     init: function () {
//         this.appendDummyInput()
//             .appendField("Make inline filter")
//             .appendField("selector")
//             .appendField(new Blockly.FieldTextInput("*"), "selector");
//         this.appendValueInput("filter")
//             .setCheck(null)
//             .appendField("filter");
//         this.setInputsInline(true);
//         this.setOutput(true, null);
//         this.setColour(300);
//         this.setTooltip("");
//         this.setHelpUrl("");
//     }
// };

Blockly.Blocks['ascii_writer_make_inline_filter'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make inline filter")
            .appendField("selector")
            .appendField(new Blockly.FieldTextInput("*"), "selector");
        this.appendStatementInput("filter")
            .setCheck(null)
            .appendField("Text filter");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

// Blockly.Blocks['ascii_writer_make_block_filter'] = {
//     init: function () {
//         this.appendDummyInput()
//             .appendField("Make block filter")
//             .appendField("selector")
//             .appendField(new Blockly.FieldTextInput("*"), "selector");
//         this.appendValueInput("filter")
//             .setCheck(null)
//             .appendField("filter");
//         this.setInputsInline(true);
//         this.setOutput(true, null);
//         this.setColour(300);
//         this.setTooltip("");
//         this.setHelpUrl("");
//     }
// };

Blockly.Blocks['ascii_writer_make_block_filter'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make block filter")
            .appendField("selector")
            .appendField(new Blockly.FieldTextInput("*"), "selector");
        this.appendStatementInput("filter")
            .setCheck(null)
            .appendField("fragments filter");
        this.appendDummyInput()
            .appendField("white space before")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_before")
            .appendField("white space after")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_after");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_block_filter__image'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make block filter for images")
            .appendField("selector")
            .appendField(new Blockly.FieldTextInput("img"), "selector");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("TRUE"), "apply_dithering")
            .appendField("Apply dithering filter");
        this.appendDummyInput()
            .appendField("white space before")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_before")
            .appendField("white space after")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_after");
        this.appendDummyInput()
            .appendField("Base URL")
            .appendField(new Blockly.FieldTextInput(""), "base_url");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

// Make gitlab url
// only path necessary
Blockly.Blocks['ascii_writer_gitlab_raw_url'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Make Gitlab url for path")
            .appendField(new Blockly.FieldTextInput(""), "path");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_image_layer'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make image layer")
        this.appendValueInput("src")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("URL");
        this.appendDummyInput()
            .appendField("Width")
            .appendField(new Blockly.FieldNumber(50, 0, Infinity, 1), "width")
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("TRUE"), "apply_dithering")
            .appendField("Apply dithering filter");
            this.setInputsInline(false);
            this.setOutput(true, null);
            this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_block_filter__doubled_header'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make block filter for a doubled header")
            .appendField("selector")
            .appendField(new Blockly.FieldTextInput("h1"), "selector");
        this.appendDummyInput()
            .appendField(new Blockly.FieldCheckbox("TRUE"), "uppercase")
            .appendField("Transform to uppercase");
        this.appendDummyInput()
            .appendField("white space before")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_before")
            .appendField("white space after")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_after");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_block_filter__underline'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make block for underlined text");
        this.appendDummyInput()
            .appendField("selector")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldTextInput("h1"), "selector");
        this.appendValueInput("underline_chars")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Underline char(s)");
        this.appendStatementInput("filter")
            .setCheck(null)
            .appendField("text filter");
        this.appendValueInput("geometry")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("geometry");
        this.appendDummyInput()
            .appendField("white space before")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_before");
        this.appendDummyInput()
            .appendField("white space after")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_after");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_block_filter__table'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make block filter for a tables");
        this.appendValueInput("geometry")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("geometry");
        this.appendDummyInput()
            .appendField("white space before")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_before");
        this.appendDummyInput()
            .appendField("white space after")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "whitespace_after");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_pattern_kernel_sinus'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Sinus");
        this.appendValueInput("period")
            .setCheck("Number")
            .appendField("period");
        this.appendValueInput("amplitude")
            .setCheck("Number")
            .appendField("amplitude");
        this.appendValueInput("offset t")
            .setCheck("Number")
            .appendField("offset t");
        this.appendValueInput("y")
            .setCheck(null)
            .appendField("Y (line)");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_pattern_kernel_cosinus'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Cosinus");
        this.appendValueInput("period")
            .setCheck("Number")
            .appendField("period");
        this.appendValueInput("amplitude")
            .setCheck("Number")
            .appendField("amplitude");
        this.appendValueInput("offset t")
            .setCheck("Number")
            .appendField("offset t");
        this.appendValueInput("y")
            .setCheck(null)
            .appendField("Y (line)");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_pattern_kernel_sharkteeth'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Sharkteeth");
        this.appendValueInput("period")
            .setCheck("Number")
            .appendField("period");
        this.appendValueInput("amplitude")
            .setCheck("Number")
            .appendField("amplitude");
        this.appendValueInput("y")
            .setCheck(null)
            .appendField("Y (line)");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_make_fragment'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Make fragment with");
        this.appendValueInput("text")
            .setCheck(null)
            .appendField("text");
        this.appendValueInput("geometry")
            .setCheck(null)
            .appendField("geometry");
        this.setInputsInline(false);
        this.setOutput(true, null)
        this.setColour(270);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_fragments_prepend_fragment'] = {
    init: function () {
        this.appendValueInput("fragment")
            .setCheck(null)
            .appendField("Insert fragment at the start");
        this.appendValueInput("fragments")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("fragments");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_fragments_append_fragment'] = {
    init: function () {
        this.appendValueInput("fragment")
            .setCheck(null)
            .appendField("Add fragment at the end");
        this.appendValueInput("fragments")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("fragments");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_fragments_add_geometry'] = {
    init: function () {
        this.appendValueInput("geometry")
            .setCheck(null)
            .appendField("Add geometry to fragments");
        this.appendValueInput("fragments")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("fragments");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_filter_return'] = {
    init: function () {
        this.appendValueInput("to_return")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Return");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setColour(270);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_filter_input_text'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Extracted text");
        this.setInputsInline(false);
        this.setOutput(true, "String");
        this.setColour(60);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_filter_line_break'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Line breaks")
            .appendField(new Blockly.FieldNumber(1, 1, Infinity, 1), "amount");
        this.setInputsInline(false);
        this.setOutput(true, "String");
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_filter_input_fragments'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Fragments");
        this.setInputsInline(false);
        this.setOutput(true, "String");
        this.setColour(60);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_frame_textbox'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Textframe");
        this.appendValueInput("width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Width");
        this.appendValueInput("geometry")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("Geometry");
        this.setInputsInline(false);
        this.setOutput(true, "String");
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


Blockly.Blocks['ascii_writer_line_based_effect'] = {
    init: function () {
        this.appendValueInput("commands")
            .setCheck(null)
            .appendField("Line based effect");
        this.setInputsInline(false);
        this.setOutput(true, "Number");
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


Blockly.Blocks['ascii_writer_line'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Linenumber");
        this.setInputsInline(false);
        this.setOutput(true, "Number");
        this.setColour(60);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_frame'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Text frame");
        this.appendValueInput("width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("width");
        this.appendValueInput("height")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("height");
        this.appendValueInput("content")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("content");
        this.appendValueInput("geometry")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("geometry");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


Blockly.Blocks['ascii_writer_simple_text_frame'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Simple text frame");
        this.appendValueInput("width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("width");
        this.appendValueInput("height")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("height");
        this.appendValueInput("text")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("text");
        this.appendDummyInput()
            .appendField("Align ")
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField(new Blockly.FieldDropdown([["left","left"], ["center","center"], ["right","right"]]), "align");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


Blockly.Blocks['ascii_writer_frame_multicolumn'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Multicolumn Textframe");
        this.appendValueInput("width")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("width");
        this.appendValueInput("height")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("height");
        this.appendValueInput("columns")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("columns");
        this.appendValueInput("column_gap")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("column gap");
        this.appendValueInput("content")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("content");
        this.appendValueInput("geometry")
            .setCheck(null)
            .setAlign(Blockly.ALIGN_RIGHT)
            .appendField("geometry");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_region_create'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Create flow")
            .appendField(new Blockly.FieldTextInput("headers"), "region");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_region_flow_from'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Flow from")
            .appendField(new Blockly.FieldTextInput("headers"), "region");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_region_get_next_key'] = {
    init: function () {
        this.appendDummyInput()
            .appendField("Get next key")
            .appendField(new Blockly.FieldTextInput("headers"), "region");
        this.setInputsInline(false);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_region_flow_into'] = {
    init: function () {
        this.appendValueInput("fragment")
            .setCheck(null)
            .appendField("Flow fragments into")
            .appendField(new Blockly.FieldTextInput("headers"), "region");
        this.setInputsInline(false);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};


Blockly.Blocks['ascii_writer_draw_layer_border'] = {
    init: function() {
      this.appendValueInput("layer")
          .setCheck(null)
          .appendField("Draw layer border");
      this.setInputsInline(false);
      this.setOutput(true, null);
      this.setColour(300);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };

Blockly.Blocks['ascii_writer_frame_list__frame'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("Textframe")
            .appendField("X")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "x")
            .appendField("Y")
            .appendField(new Blockly.FieldNumber(0, 0, Infinity, 1), "y")
            .appendField("Height")
            .appendField(new Blockly.FieldNumber(60, 0, Infinity, 1), "height");
        this.setInputsInline(true);
        this.setOutput(true, null);
        this.setColour(300);
        this.setTooltip("");
        this.setHelpUrl("");
    }
};

Blockly.Blocks['ascii_writer_multiframe_layer'] = {
    init: function() {
      this.appendDummyInput()
          .appendField("Multiple textframe layer");
      this.appendDummyInput()
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("Width")
          .appendField(new Blockly.FieldNumber(60, 0, Infinity, 1), "width");
      this.appendValueInput("frames")
          .setCheck(null)
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("frames");
      this.appendValueInput("content")
          .setCheck(null)
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("content");
      this.appendValueInput("geometry")
          .setCheck(null)
          .setAlign(Blockly.ALIGN_RIGHT)
          .appendField("geometry");
      this.setInputsInline(false);
      this.setOutput(true, null);
      this.setColour(300);
   this.setTooltip("");
   this.setHelpUrl("");
    }
  };