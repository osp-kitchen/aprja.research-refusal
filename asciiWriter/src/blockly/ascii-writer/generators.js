import * as Blockly from 'blockly';
import asciiWriter from '../../asciiWriter';

Blockly.JavaScript['ascii_writer_drawing'] = function (block) {
  // var statements_commands = Blockly.JavaScript.statementToCode(block, 'commands');
  var statements_commands = Blockly.JavaScript.blockToCode(block.getInputTargetBlock('commands'));
  // TODO: Assemble JavaScript into code variable.
  var code = "import asciiWriter from './asciiWriter/index.js';\n\n(async function () {\nlet regions = {\n  'images': new asciiWriter.render.Registry()\n};\n\n" + statements_commands + "})();\n";
  return code;
};

Blockly.JavaScript['ascii_writer_apply_pattern'] = function (block) {
  var value_layer = Blockly.JavaScript.valueToCode(block, 'layer', Blockly.JavaScript.ORDER_ATOMIC);
  var value_pattern = Blockly.JavaScript.valueToCode(block, 'pattern', Blockly.JavaScript.ORDER_ATOMIC);
  var value_marker = Blockly.JavaScript.valueToCode(block, 'marker', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.visit(' + value_layer + ', ' + value_pattern + ', ' + value_marker + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_make_layer'] = function (block) {
  var value_width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.make_layer(' + value_width + ', ' + value_height + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_pattern_sinus_vertical'] = function (block) {
  var value_period = Blockly.JavaScript.valueToCode(block, 'period', Blockly.JavaScript.ORDER_ATOMIC);
  var value_amplitude = Blockly.JavaScript.valueToCode(block, 'amplitude', Blockly.JavaScript.ORDER_ATOMIC);
  var value_offset = Blockly.JavaScript.valueToCode(block, 'offset', Blockly.JavaScript.ORDER_ATOMIC);
  var value_offset_t = Blockly.JavaScript.valueToCode(block, 'offset_t', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.patterns.sinus_vertical(' + value_period + ', ' + value_amplitude + ', ' + value_offset_t + ', ' + value_offset + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_pattern_cosinus_vertical'] = function (block) {
  var value_period = Blockly.JavaScript.valueToCode(block, 'period', Blockly.JavaScript.ORDER_ATOMIC);
  var value_amplitude = Blockly.JavaScript.valueToCode(block, 'amplitude', Blockly.JavaScript.ORDER_ATOMIC);
  var value_offset = Blockly.JavaScript.valueToCode(block, 'offset', Blockly.JavaScript.ORDER_ATOMIC);
  var value_offset_t = Blockly.JavaScript.valueToCode(block, 'offset_t', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.patterns.cosinus_vertical(' + value_period + ', ' + value_amplitude + ', ' + value_offset_t + ', ' + value_offset + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_draw_layer_border'] = function (block) {
  var value_layer = Blockly.JavaScript.valueToCode(block, 'layer', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.visit(' + value_layer + ', asciiWriter.patterns.draw_layer_border(), "")';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_marker_text'] = function (block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.marks.text(' + value_text + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_marker_repeating_text'] = function (block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.marks.text(' + value_text + ', true)';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_show_layer'] = function (block) {
  var value_layer = Blockly.JavaScript.valueToCode(block, 'layer', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '(() => {\nlet pre = ((PREVIEW_CONTAINER) ? PREVIEW_CONTAINER : document.body).appendChild(document.createElement("pre"));\nasciiWriter.printInElement(' + value_layer + ', pre);\nasciiWriter.render.renderImages(pre, regions);\n})();\n'
  return code;
};

Blockly.JavaScript['ascii_writer_merge_layer'] = function (block) {
  var value_layers = Blockly.JavaScript.valueToCode(block, 'layers', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.merge(' + value_layers + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_text_effects_space'] = function (block) {
  var value_space = Blockly.JavaScript.valueToCode(block, 'space', Blockly.JavaScript.ORDER_ATOMIC);
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.textEffects.space(' + value_space + ')(' + value_text + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_text_effects_double'] = function (block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.textEffects.double(' + value_text + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_text_effects_uppercase'] = function (block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.textEffects.uppercase(' + value_text + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_text_effects_lowercase'] = function (block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.textEffects.lowercase(' + value_text + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_geometry_offset'] = function (block) {
  var value_x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  var value_layer = Blockly.JavaScript.valueToCode(block, 'layer', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.geometry.offset(' + value_x + ', ' + value_y + ')(' + value_layer + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_text_column'] = function (block) {
  var value_column_width = Blockly.JavaScript.valueToCode(block, 'column_width', Blockly.JavaScript.ORDER_ATOMIC);
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.text.make_column(' + value_text + ', ' + value_column_width + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_first_then'] = function (block) {
  var value_first = Blockly.JavaScript.valueToCode(block, 'first', Blockly.JavaScript.ORDER_ATOMIC);
  var value_then = Blockly.JavaScript.valueToCode(block, 'then', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.first_then(' + value_first + ', ' + value_then + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_create_geometries'] = function (block) {
  var value_inset = Blockly.JavaScript.valueToCode(block, 'inset', Blockly.JavaScript.ORDER_ATOMIC);
  var value_shorten = Blockly.JavaScript.valueToCode(block, 'shorten', Blockly.JavaScript.ORDER_ATOMIC);
  var value_prefix = Blockly.JavaScript.valueToCode(block, 'prefix', Blockly.JavaScript.ORDER_ATOMIC);
  var value_postfix = Blockly.JavaScript.valueToCode(block, 'postfix', Blockly.JavaScript.ORDER_ATOMIC);
  var checkbox_avoid_column_break_inside = (block.getFieldValue('avoid_column_break_inside') == 'TRUE') ? true : null;
  var checkbox_avoid_column_break_after = (block.getFieldValue('avoid_column_break_after') == 'TRUE') ? true : null;
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.make_geometries(' + value_inset + ', ' + value_shorten + ', ' + value_prefix + ', ' + value_postfix + ', ' + checkbox_avoid_column_break_inside + ', ' + checkbox_avoid_column_break_after + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_get_markdown'] = function (block) {
  var text_path = block.getFieldValue('path');
  // TODO: Assemble JavaScript into code variable.
  var code = 'await asciiWriter.markdown.retreiveMarkdownFromGitlabAPI("https://gitlab.com", 23857158, "' + text_path + '")';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_flow_markdown'] = function (block) {
  var value_markdown = Blockly.JavaScript.valueToCode(block, 'markdown', Blockly.JavaScript.ORDER_ATOMIC);
  var value_filters = Blockly.JavaScript.valueToCode(block, 'filters', Blockly.JavaScript.ORDER_ATOMIC);
  var value_frame = Blockly.JavaScript.valueToCode(block, 'frame', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '(async function () {\n'
    + '  asciiWriter.render.render(' + value_markdown + ', ' + value_filters + ', ' + value_frame + ');'
    + '})();\n';
  return code;
};

Blockly.JavaScript['ascii_writer_render_markdown'] = function (block) {
  var value_markdown = Blockly.JavaScript.valueToCode(block, 'markdown', Blockly.JavaScript.ORDER_ATOMIC);
  var value_filters = Blockly.JavaScript.valueToCode(block, 'filters', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'await asciiWriter.render.renderMarkdown(' + value_markdown + ', ' + value_filters + ', regions)';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

// Blockly.JavaScript['ascii_writer_make_inline_filter'] = function(block) {
//   var text_selector = block.getFieldValue('selector');
//   var value_filter = Blockly.JavaScript.valueToCode(block, 'filter', Blockly.JavaScript.ORDER_ATOMIC);
//   // TODO: Assemble JavaScript into code variable.
//   var code = 'new InlineFilter(' + text_selector + ', ' + value_filter + ')';
//   // TODO: Change ORDER_NONE to the correct strength.
//   return [code, Blockly.JavaScript.ORDER_NONE];
// };

Blockly.JavaScript['ascii_writer_make_inline_filter'] = function (block) {
  var text_selector = block.getFieldValue('selector');
  var statements_filter = Blockly.JavaScript.statementToCode(block, 'filter');
  // TODO: Assemble JavaScript into code variable.
  var code = 'new asciiWriter.render.InlineFilter("' + text_selector + '", (filter_text, filter_node) => {\n' + statements_filter + '\n})';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

// Blockly.JavaScript['ascii_writer_make_block_filter'] = function(block) {
//   var text_selector = block.getFieldValue('selector');
//   var value_filter = Blockly.JavaScript.valueToCode(block, 'filter', Blockly.JavaScript.ORDER_ATOMIC);
//   // TODO: Assemble JavaScript into code variable.
//   var code = 'new BlockFilter(' + text_selector + ', ' + value_filter + ')';
//   // TODO: Change ORDER_NONE to the correct strength.
//   return [code, Blockly.JavaScript.ORDER_NONE];
// };

Blockly.JavaScript['ascii_writer_make_block_filter'] = function (block) {
  var text_selector = block.getFieldValue('selector');
  var statements_filter = Blockly.JavaScript.statementToCode(block, 'filter');
  var number_whitespace_before = block.getFieldValue('whitespace_before');
  var number_whitespace_after = block.getFieldValue('whitespace_after');
  // TODO: Assemble JavaScript into code variable.
  var code = 'new asciiWriter.render.BlockFilter("' + text_selector + '", function (filter_fragments, filter_node, filter_regions) {\n' + statements_filter + '\n}, true, ' + number_whitespace_before + ', ' + number_whitespace_after + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_make_block_filter__image'] = function (block) {
  var text_selector = block.getFieldValue('selector');
  var number_whitespace_before = block.getFieldValue('whitespace_before');
  var number_whitespace_after = block.getFieldValue('whitespace_after');
  var checkbox_apply_dithering = (block.getFieldValue('apply_dithering') == 'TRUE') ? true : null;
  var text_base_url = block.getFieldValue('base_url');
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.makeImageFilter("' + text_selector + '", ' + number_whitespace_before + ', ' + number_whitespace_after + ', ' + ((checkbox_apply_dithering) ? 'true' : 'false') + ', "' + text_base_url + '")';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_make_image_layer'] = function (block) {
  var value_src = Blockly.JavaScript.valueToCode(block, 'src', Blockly.JavaScript.ORDER_ATOMIC);
  var number_width = block.getFieldValue('width');
  var checkbox_apply_dithering = (block.getFieldValue('apply_dithering') == 'TRUE') ? true : null;
  // TODO: Assemble JavaScript into code variable.
  var code = 'await asciiWriter.render.makeImageLayer(' + value_src + ', ' + number_width + ', ' + ((checkbox_apply_dithering) ? 'true' : 'false') + ', regions)';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};


Blockly.JavaScript['ascii_writer_gitlab_raw_url'] = function(block) {
  var text_path = block.getFieldValue('path');
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.gitlab.make_url_api_raw("' + text_path + '")';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_make_block_filter__doubled_header'] = function (block) {
  var text_selector = block.getFieldValue('selector');
  var number_whitespace_before = block.getFieldValue('whitespace_before');
  var number_whitespace_after = block.getFieldValue('whitespace_after');
  var checkbox_uppercase = (block.getFieldValue('uppercase') == 'TRUE') ? true : null;
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.makeDoubledHeaderFilter("' + text_selector + '", ' + number_whitespace_before + ', ' + number_whitespace_after + ', ' + ((checkbox_uppercase) ? 'true' : 'false') + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_make_block_filter__underline'] = function (block) {
  var text_selector = block.getFieldValue('selector');
  var value_underline_chars = Blockly.JavaScript.valueToCode(block, 'underline_chars', Blockly.JavaScript.ORDER_ATOMIC);
  var number_whitespace_before = block.getFieldValue('whitespace_before');
  var number_whitespace_after = block.getFieldValue('whitespace_after');
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  var statements_filter = Blockly.JavaScript.statementToCode(block, 'filter');
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.makeUnderlineFilter("' + text_selector + '", ' + value_underline_chars + ', ' + number_whitespace_before + ', ' + number_whitespace_after + ', ' + value_geometry + ', (filter_text) => {\n' + statements_filter + '\n})';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_make_block_filter__table'] = function (block) {
  var number_whitespace_before = block.getFieldValue('whitespace_before');
  var number_whitespace_after = block.getFieldValue('whitespace_after');
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.table.makeTableFilter(' + number_whitespace_before + ', ' + number_whitespace_after + ', ' + value_geometry + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_pattern_kernel_sinus'] = function (block) {
  var value_period = Blockly.JavaScript.valueToCode(block, 'period', Blockly.JavaScript.ORDER_ATOMIC);
  var value_amplitude = Blockly.JavaScript.valueToCode(block, 'amplitude', Blockly.JavaScript.ORDER_ATOMIC);
  var value_offset_t = Blockly.JavaScript.valueToCode(block, 'offset t', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.patternKernels.sinus(' + value_period + ', ' + value_amplitude + ', ' + value_offset_t + ')(' + value_y + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_pattern_kernel_cosinus'] = function (block) {
  var value_period = Blockly.JavaScript.valueToCode(block, 'period', Blockly.JavaScript.ORDER_ATOMIC);
  var value_amplitude = Blockly.JavaScript.valueToCode(block, 'amplitude', Blockly.JavaScript.ORDER_ATOMIC);
  var value_offset_t = Blockly.JavaScript.valueToCode(block, 'offset t', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.patternKernels.cosinus(' + value_period + ', ' + value_amplitude + ', ' + value_offset_t + ')(' + value_y + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_pattern_kernel_sharkteeth'] = function (block) {
  var value_period = Blockly.JavaScript.valueToCode(block, 'period', Blockly.JavaScript.ORDER_ATOMIC);
  var value_amplitude = Blockly.JavaScript.valueToCode(block, 'amplitude', Blockly.JavaScript.ORDER_ATOMIC);
  var value_y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.patternKernels.sharkteeth(' + value_period + ', ' + value_amplitude + ')(' + value_y + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

/** Fragment manipulation */
 
Blockly.JavaScript['ascii_writer_flatten_texts'] = function (block) {
  var value_fragments = Blockly.JavaScript.valueToCode(block, 'fragments', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.flatten_texts(' + value_fragments + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};


Blockly.JavaScript['ascii_writer_make_fragment'] = function (block) {
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.render.make_fragment(' + value_text + ', ' + value_geometry + ')';

  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_fragments_prepend_fragment'] = function (block) {
  var value_fragments = Blockly.JavaScript.valueToCode(block, 'fragments', Blockly.JavaScript.ORDER_ATOMIC);
  var value_fragment = Blockly.JavaScript.valueToCode(block, 'fragment', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = value_fragments + '.unshift(' + value_fragment + ');\n';

  return code;
};

Blockly.JavaScript['ascii_writer_fragments_append_fragment'] = function (block) {
  var value_fragments = Blockly.JavaScript.valueToCode(block, 'fragments', Blockly.JavaScript.ORDER_ATOMIC);
  var value_fragment = Blockly.JavaScript.valueToCode(block, 'fragment', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = value_fragments + '.push(' + value_fragment + ');\n';

  return code;
};

Blockly.JavaScript['ascii_writer_fragments_add_geometry'] = function (block) {
  var value_fragments = Blockly.JavaScript.valueToCode(block, 'fragments', Blockly.JavaScript.ORDER_ATOMIC);
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = value_fragments + ' = asciiWriter.render.fragments_add_geometries(' + value_fragments + ', ' + value_geometry + ');\n';
  // TODO: Change ORDER_NONE to the correct strength.
  return code;
};

Blockly.JavaScript['ascii_writer_filter_return'] = function (block) {
  var value_to_return = Blockly.JavaScript.valueToCode(block, 'to_return', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'return ' + value_to_return + ';\n';
  return code;
};

Blockly.JavaScript['ascii_writer_filter_input_text'] = function (block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'filter_text';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_filter_input_fragments'] = function (block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'filter_fragments';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_filter_line_break'] = function(block) {
  var number_amount = block.getFieldValue('amount');
  // TODO: Assemble JavaScript into code variable.
  var code = '"' + Array(number_amount).fill('\\n').join('') + '"';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_frame_textbox'] = function(block) {
  var value_width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.text.make_column_fragments(' + value_width + ', ' + value_geometry + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_line_based_effect'] = function(block) {
  var value_commands = Blockly.JavaScript.valueToCode(block, 'commands', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '(line) => (' + value_commands + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_line'] = function (block) {
  // TODO: Assemble JavaScript into code variable.
  var code = 'line';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_frame'] = function(block) {
  var value_width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var value_content = Blockly.JavaScript.valueToCode(block, 'content', Blockly.JavaScript.ORDER_ATOMIC);
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.text.make_textframe(' + value_width + ', ' + value_height + ', ' + value_content + ', ' + value_geometry + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_simple_text_frame'] = function(block) {
  var value_width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var value_text = Blockly.JavaScript.valueToCode(block, 'text', Blockly.JavaScript.ORDER_ATOMIC);
  var dropdown_align = block.getFieldValue('align');
  var value_align;
  if (dropdown_align == 'right') {
    value_align = 'asciiWriter.text.TEXT_ALIGN_RIGHT';
  }
  else if (dropdown_align == 'center') {
    value_align = 'asciiWriter.text.TEXT_ALIGN_CENTER';
  }
  else {
    value_align = 'asciiWriter.text.TEXT_ALIGN_LEFT';
  }
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.text.make_simple_text_frame(' + value_width + ', ' + value_height + ', ' + value_text + ', ' + value_align + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_frame_multicolumn'] = function(block) {
  var value_width = Blockly.JavaScript.valueToCode(block, 'width', Blockly.JavaScript.ORDER_ATOMIC);
  var value_height = Blockly.JavaScript.valueToCode(block, 'height', Blockly.JavaScript.ORDER_ATOMIC);
  var value_columns = Blockly.JavaScript.valueToCode(block, 'columns', Blockly.JavaScript.ORDER_ATOMIC);
  var value_column_gap = Blockly.JavaScript.valueToCode(block, 'column_gap', Blockly.JavaScript.ORDER_ATOMIC);
  var value_content = Blockly.JavaScript.valueToCode(block, 'content', Blockly.JavaScript.ORDER_ATOMIC);
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.text.make_multicolumn_textframe(' + value_width + ', ' + value_height + ', ' + value_columns + ', ' + value_column_gap + ', ' + value_content + ', ' + value_geometry + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_region_create'] = function(block) {
  var text_region = block.getFieldValue('region');
  // TODO: Assemble JavaScript into code variable.
  var code = 'regions["' + text_region +  '"] = new asciiWriter.render.Registry();\n';
  // TODO: Change ORDER_NONE to the correct strength.
  return code;
};


Blockly.JavaScript['ascii_writer_region_flow_from'] = function(block) {
  var text_region = block.getFieldValue('region');
  // TODO: Assemble JavaScript into code variable.
  var code = 'regions["' + text_region +  '"].fragments';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_region_flow_into'] = function(block) {
  var text_region = block.getFieldValue('region');
  var value_fragment = Blockly.JavaScript.valueToCode(block, 'fragment', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'regions["' + text_region +  '"].register(' + value_fragment + ');\n';
  // TODO: Change ORDER_NONE to the correct strength.
  return code;
};

Blockly.JavaScript['ascii_writer_region_get_next_key'] = function(block) {
  var text_region = block.getFieldValue('region');
  // TODO: Assemble JavaScript into code variable.
  var code = 'regions["' + text_region + '"].getNextKey() + 1';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['ascii_writer_frame_list__frame'] = function(block) {
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_height = block.getFieldValue('height');
  // TODO: Assemble JavaScript into code variable.
  var code = 'new asciiWriter.text.TextFrame(' + number_x + ', ' + number_y + ', ' + number_height + ')';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['ascii_writer_multiframe_layer'] = function(block) {
  var number_width = block.getFieldValue('width');
  var value_frames = Blockly.JavaScript.valueToCode(block, 'frames', Blockly.JavaScript.ORDER_ATOMIC);
  var value_content = Blockly.JavaScript.valueToCode(block, 'content', Blockly.JavaScript.ORDER_ATOMIC);
  var value_geometry = Blockly.JavaScript.valueToCode(block, 'geometry', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'asciiWriter.text.make_multiframe_text_layer(' + value_frames + ', ' + number_width + ', ' + value_content + ', ' + value_geometry + ')';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};