export const API_URL = (PRODUCTION) ? "/api/" : "http://localhost:5000/api/";

export const api_url = (tail) => {
  return `${API_URL}${tail}`;
}

export const get = (url, data) => {
  if (data) {
    const URLData = new URLSearchParams(data);
    url += '?' + URLData.toString();
  }

  return new Promise((resolve, reject) => {
    fetch(api_url(url), {
      method: "GET"
    }).then((response) => {
      if (response.ok) {
          response.json()
          .then(resolve)
          .catch(reject);
      }
      else {
        reject();
      }
    }).catch(reject);
  });
}

export const get_text = (url, data) => {
  if (data) {
    const URLData = new URLSearchParams(data);
    url += '?' + URLData.toString();
  }

  return new Promise((resolve, reject) => {
    fetch(api_url(url), {
      method: "GET"
    }).then((response) => {
      if (response.ok) {
          response.text()
          .then(resolve)
          .catch(reject);
      }
      else {
        reject();
      }
    }).catch(reject);
  });
}

const _post = (url, data) => {
  return new Promise((resolve, reject) => {
    const postData = new FormData();

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        if (Array.isArray(data[key])) {
          data[key].forEach((v) => postData.append(key, v));
        } else {
          postData.append(key, data[key]);
        }
      }
    }

    fetch(api_url(url), {
      method: "POST",
      body: postData
    }).then(resolve).catch(reject);
  });
}

// TODO generalize API call function
export const post = (url, data) => {
  return new Promise((resolve, reject) => {
    _post(url, data).then((response) => {
      if (response.ok) {
        response.json()
          .then((data) => resolve(data.results))
          .catch(reject);
      } else {
        reject();
      }
    }).catch(reject);
  });
}

export default { api_url, get, get_text, post }