import { offset } from './geometry.js';
import { make_layer, merge } from './index.js';
import { isCallable } from './isCallable.js';
import { make_fragment, make_geometries, Whitespace } from './render.js';

export const CHARCODE_NONBREAKINGSPACE = 160;
export const CHARCODE_ZEROWIDTHSPACE = 8203;
export const CHARCODE_ZEROWIDTHJOINER = 8205;

export const TEXT_ALIGN_LEFT = 0;
export const TEXT_ALIGN_CENTER = 1;
export const TEXT_ALIGN_RIGHT = 2;

export function is_empty_line (line) {
  if (line === null) {
    return true;
  } else {
    let chars = line[0];
    for (let i = 0; i < chars.length; i++) {
      if (chars[i] !== '' && chars[i] !== ' ' && chars[i] !== null) {
        return false;
      }
    }
  
    return true;
  }
}


export function wrap_line (text, width) {
  let whitespace_regex = /\s/,
      is_breakable_whitespace = (char) => whitespace_regex.test(char) && char.charCodeAt(0) !== CHARCODE_NONBREAKINGSPACE,
      is_breakable = (char) => (char == '-'),
      line, remainder;
  
  // Is there a linebreak within the current
  // desired width. If so return. 
  const linebreakIndex = text.substr(0, width + 2).indexOf('\n');

  if (linebreakIndex > -1) {
    line = text.substr(0, linebreakIndex);
    remainder = text.substr(linebreakIndex + 1);
  } else if (text.length <= width) {
    // Text is already less wide than desired width
    line = text;
    remainder = null;
  } else if (is_breakable_whitespace(text[width])) {
      line = text.substr(0, width);
      remainder = text.substr(width + 1);
  }
  // Trim untill there is a suitable breaking point
  else {
    for (let index = (width - 1); index > 0; index--) {
      if (text[index] == '\n' || is_breakable_whitespace(text[index])) {
        line = text.substr(0, index);
        remainder = text.substr(index + 1)
        break;
      }
      else if (is_breakable(text[index])) {
        line = text.substr(0, index + 1);
        remainder = text.substr(index + 1)
        break;
      }      
    }

    if (!line) {
      line = text.substr(0, width);
      remainder = text.substr(width).trim();
    }

    if (remainder) {
      while (remainder.startsWith(' ')) {
        remainder = remainder.substr(1);
      }
    }
  }


  return [line, remainder];
}


export function make_column (text, line_width=50, line_offset=0) {
  let lines = [], line,
      remaining = text,
      widthCallable = isCallable(line_width),
      offsetCallable = isCallable(line_offset);

  while (remaining) {
    [ line, remaining ] = wrap_line(remaining, (widthCallable) ? line_width(lines.length) : line_width)

    // Transform to array, insert offset is necessary and add to lines
    lines.push(Array(offsetCallable ? line_offset(lines.length) : line_offset).fill(null).concat(line.split('')));
  }

  return lines;
}


let resolve_geometry = (geometry, line) => ({
    inset: (isCallable(geometry.inset)) ? Math.round(geometry.inset(line)) : geometry.inset,
    shorten: (isCallable(geometry.shorten)) ? Math.round(geometry.shorten(line)) : geometry.shorten,
    prefix: (isCallable(geometry.prefix)) ? geometry.prefix(line) : geometry.prefix,
    postfix: (isCallable(geometry.postfix)) ? geometry.postfix(line) : geometry.postfix,
    avoid_column_break_inside: (isCallable(geometry.avoid_column_break_inside)) ? geometry.avoid_column_break_inside(line) : geometry.avoid_column_break_inside,
    avoid_column_break_after: (isCallable(geometry.avoid_column_break_after)) ? geometry.avoid_column_break_after(line) : geometry.avoid_column_break_after
  });

let resolve_geometries = (geometries, line, fragment_line) => {
  let prefix = '', postfix = '', shorten = 0, avoid_column_break_inside = null, avoid_column_break_after = null;

  for (let g = 0; g < geometries.length; g++) {
    const geometry = resolve_geometry(geometries[g], line, fragment_line);
    let local_inset = geometry.inset,
        local_shorten = geometry.shorten,
        local_prefix = geometry.prefix,
        local_postfix = geometry.postfix;

    if (geometry.avoid_column_break_inside === false || geometry.avoid_column_break_inside === true) {
      avoid_column_break_inside = geometry.avoid_column_break_inside
    }
    
    if (geometry.avoid_column_break_after === false || geometry.avoid_column_break_after === true) {
      avoid_column_break_after = geometry.avoid_column_break_after
    }

    // console.log(local_inset, local_prefix, geometries[g]);

    prefix += Array(local_inset).fill(' ').join('') + local_prefix;
    postfix = local_postfix + postfix;
    shorten += local_shorten;
  }
  
  return [ prefix, postfix, shorten, avoid_column_break_inside, avoid_column_break_after ];
}

/**
 * 
 * @param {*} line_width 
 * @param {*} column_geometry 
 * @returns [ [ [ str:char, ...], bool:nonbreakingblock?, bool:avoid_break_after?, bool:last_line_of_fragment? ]]
 */
export function make_column_fragments (line_width, column_geometry, align = TEXT_ALIGN_LEFT) {
  return function (fragments) {
    let line, remaining, fragment_geometries, geometries, lines = [], line_counter = 0;
        
    for (let i = 0; i < fragments.length; i++) {
      if (fragments[i] instanceof Whitespace) {
        let height = fragments[i].height;

        if (fragments[i + 1] instanceof Whitespace) {
          height = fragments[i].collapse(fragments[i+1]).height
          i++;
        }

        for (let w = 0; w < height; w++) {
          lines.push(null);
        }
      } else {
        [ remaining, fragment_geometries ] = fragments[i];
        geometries = column_geometry.concat(fragment_geometries);
        let fragment_line_counter = 0;
        
        while (remaining) {
          let [ prefix, postfix, shorten, avoid_column_break_inside, avoid_column_break_after ] = resolve_geometries(geometries, lines.length, fragment_line_counter);
          let width = line_width - shorten - prefix.length - postfix.length;
          
          // If the fragment is callable do so now with the width of the textbox first
          if (isCallable(remaining)) {
            remaining = remaining(width);
          }
  
          [ line , remaining ] = wrap_line(remaining, width);
  
          if (postfix) {
            // Fill up line in case of a postfix.
            line = line + Array(width - line.length).fill(' ').join('') + postfix
          }
          if (prefix) {
            line = prefix + line;
          }
    
          lines.push([line.split(''), avoid_column_break_inside, avoid_column_break_after, !remaining ]);
          
          fragment_line_counter++;
        }
      }

    }
  
    return align_lines(lines, align, line_width);
  }
}


function extract_text_from_lines (lines) {
  return lines.map((line) => (line === null) ? [] : line[0]);
}

function align_lines (lines, align, width) {
  if (align == TEXT_ALIGN_RIGHT) {
    return lines.map((line) => {
      line[0] = Array(Math.max(0, width - line[0].length)).fill(null).concat(line[0]);
      return line;
    });
  }
  else if (align == TEXT_ALIGN_CENTER) {
    console.warn('Center alignment not yet implemented.');
  }
  return lines;
}

export function make_textframe (width, height, fragments, geometry, align = TEXT_ALIGN_LEFT) {
  let lines = make_column_fragments(width, geometry, align)(fragments);

  if (height !== null && lines.length > height) {
    lines = lines.slice(0, height);
  }

  return extract_text_from_lines(lines);
}

function last_line_in_column (line) {
  return line[3];
}

function avoid_column_break_inside (line) {
  return line[1];
}

function avoid_column_break_after (line) {
  return line[2];
}

function expand_column_gaps (column_count, column_gap) {
  let column_gaps = [],
      total_gap_width = 0;
  if (typeof column_gap == 'number') {
    column_gaps = new Array(column_count - 1).fill(column_gap);
    total_gap_width = (column_count - 1) * column_gap;
  }
  else if (isCallable(column_gap)) {
    for (let c=0; c < (column_count - 1); c++) {
      let gap = column_gap(c);
      total_gap_width += gap;
      column_gaps.push(gap);
    }
  }
  else if (Array.isArray(column_gap)) {
    for (let c=0; c < (column_count - 1); c++) {
      let gap = column_gap[c % column_gap.length];
      console.log(gap);
      total_gap_width += gap;
      column_gaps.push(gap);
    }
  }
  console.log(total_gap_width);
  return [column_gaps, total_gap_width];
}

function calculate_column_width (frame_width, column_count, column_gap) {
  let [ column_gaps, total_gap_width ] = expand_column_gaps(column_count, column_gap);
  let column_width = Math.floor((frame_width - total_gap_width) / column_count);

  return [ column_width, column_gaps ];
}

export function make_multicolumn_textframe (width, height, column_count, column_gap, fragments, geometry) {
  let [ column_width, column_gaps ] = calculate_column_width(width, column_count, column_gap);

  let lines = make_column_fragments(column_width, geometry)(fragments);
  let column_counter = 0;
  let column_layers = [];
  let l_start = 0;
  let offset_x = 0;

  while (l_start < lines.length && column_counter < column_count) {
    while (is_empty_line(lines[l_start])) {
      // Loop to avoid having empty lines at the start of a column
      l_start += 1;
    }
    
    let l_end = l_start + height;

    if (l_end < lines.length) {      
      while (
        lines[l_end - 1] === null
        || (avoid_column_break_inside(lines[l_end - 1]) && !last_line_in_column(lines[l_end - 1]))
        || (avoid_column_break_after(lines[l_end - 1]) && last_line_in_column(lines[l_end - 1]))
      ) {
        l_end--;

        if (l_end == l_start) {
          l_end = l_start + height;
          break;
        }
      }
    }

    // Extract lines from the generated strip
    let column = extract_text_from_lines(lines.slice(l_start, Math.min(lines.length, l_end)));
    // Shift horizontally
    column = offset(offset_x, 0)(column);
    offset_x += column_width + column_gaps[Math.min(column_counter, column_gaps.length - 1)];
    // add to the layers
    column_layers.push(column);
    column_counter++;
    l_start = l_end;
  }

  return merge(column_layers);
}


export class TextFrame {
  constructor (x, y, height) {
    this.x = x
    this.y = y
    this.height = height
  }
}


export function make_multiframe_text_layer (frame_list, frame_width, fragments, geometry) {
  let lines = make_column_fragments(frame_width, geometry)(fragments);
  let frame_layers = [];
  let l_start = 0;

  while (l_start < lines.length && frame_layers.length < frame_list.length) {
    while (is_empty_line(lines[l_start])) {
      // Loop to avoid having empty lines at the start of a column
      l_start += 1;
    }
    
    let frame = frame_list[frame_layers.length],
        l_end = l_start + frame.height;

    if (l_end < lines.length) {      
      while (
        lines[l_end - 1] === null
        || (avoid_column_break_inside(lines[l_end - 1]) && !last_line_in_column(lines[l_end - 1]))
        || (avoid_column_break_after(lines[l_end - 1]) && last_line_in_column(lines[l_end - 1]))
      ) {
        l_end--;

        if (l_end == l_start) {
          l_end = l_start + height;
          break;
        }
      }
    }

    // Extract lines from the generated strip
    let frame_layer = extract_text_from_lines(lines.slice(l_start, Math.min(lines.length, l_end)));
    // Shift horizontally
    frame_layer = offset(frame.x, frame.y)(frame_layer);
    frame_layers.push(frame_layer);
    l_start = l_end;
  }

  return merge(frame_layers);
}

export const make_simple_text_frame = (width, height, text, align) => (
  make_textframe(width, height, [ make_fragment(text, make_geometries()) ], make_geometries(), align)
);

export default { wrap_line, make_column, make_column_fragments, make_textframe, make_multicolumn_textframe, TextFrame, make_multiframe_text_layer, make_simple_text_frame, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, TEXT_ALIGN_RIGHT }