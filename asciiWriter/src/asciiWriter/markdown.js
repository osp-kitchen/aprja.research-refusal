// import { marked } from './marked.min.js';
import { make_url_api_raw } from "./gitlab.js";

async function getTextFromResponse (response) {
  return new Promise((resolve) => {
    response.text().then((text) => {
      resolve(text)
    });
  });
}

export async function retreiveMarkdown (url) {
  let response = await fetch(url, { method: 'GET', mode: 'cors', credentials: 'omit' });

  if (response.status == 200) {
    return await getTextFromResponse(response);
  }
  else {
    console.log(`Unable to retreive Markdown file at ${ url }`, response.status, response);
    return false;
  }
}

export async function retreiveMarkdownFromGitlabAPI(base_url, project_id, file_path) {
  if (base_url.endsWith('/')) {
    base_url = base_url.substr(0, base_url.length - 1);
  }
  
  return await retreiveMarkdown(make_url_api_raw(file_path));
}

export function renderMarkdown (markdown) {
  return marked(markdown);
}


export default { retreiveMarkdown, retreiveMarkdownFromGitlabAPI, renderMarkdown };