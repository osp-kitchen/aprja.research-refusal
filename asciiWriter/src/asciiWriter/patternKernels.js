/**
 * 
 * @param {Number} period of the sinus, in columns (vertical) / lines (horizontal)
 * @param {Number} amplitude of the sinus in columns (vertical) / lines (horizontal)
 * @param {Number} t_offset of the sinus in columns (vertical) / lines (horizontal)
 * @returns Number
 */
export const sinus = (period=(Math.PI * 2), amplitude=1, t_offset=0) => (y) => (
  Math.sin((t_offset + y) / (period / (Math.PI * 2))) * amplitude)

export const cosinus = (period=(Math.PI * 2), amplitude=1, t_offset=0) => (y) => (
  Math.cos((t_offset + y) / (period / (Math.PI * 2))) * amplitude)

export const sharkteeth = (period, amplitude) => (y) => {
  let t = (y % period) / period; // [0 → 1]
  t -= .5; // translate down [-.5 → .5]
  t = Math.abs(t); // Inflect(?) on x axis [.5 → 0 → .5] \/
  t = .5 - t; // Invert [0 → 0.5 → 0] /\
  t *= 2; // [ 0 → 1 → 0]

  return t * amplitude;
}

export default { sinus, cosinus }