inset = (geometry, inset = 0) =>
  geometry.inset = (line) => geometry.inset(line) + (inset)


set(geometry, text)

let make_geometry = (inset=0, initial_inset = 0, shorten=0, prefix='', postfix='') => (
  { inset, initial_inset, shorten, prefix, postfix })
    


/* width is information coming from outside. */
/* inset is bubbling */