import { blank } from './marks.js';
import { sinus } from './patternKernels.js';

export function horizontal_line (y_pos) {
  return (x, y, width, height, mark) => {
    if (y == y_pos) {
      return mark();
    }
    else {
      return blank();
    }
  };
}

export function vertical_line (x_pos) {
  return (x, y, width, height, mark) => {
    if (x == x_pos) {
      return mark();
    }
    else {
      return blank();
    }
  };
}

export function sinus_vertical (period=0.2, amplitude=0.4, t_offset=0, offset=0) {
  return (x, y, width, height, mark) => {
    const middle = (width - 1) * .5,
          to_mark = Math.floor(middle + sinus(period, amplitude, t_offset)(y))
    
    if ((x - offset) == to_mark) {
      return mark();
    }
    else {
      return blank();
    }
  }
}

export function cosinus_vertical (period=0.2, amplitude=0.4, t_offset=0, offset=0) {
  return (x, y, width, height, mark) => {
    const middle = (width - 1) * .5,
          to_mark = Math.floor(middle + cosinus(period, amplitude, t_offset)(y))
    
    if ((x - offset) == to_mark) {
      return mark();
    }
    else {
      return blank();
    }
  }
}

export function sinus_horizontal (period=0.2, amplitude=0.4, t_offset=0, offset=0) {
  return (x, y, width, height, mark) => {
    const middle = (height - 1) * .5,
          to_mark = Math.floor(middle + sinus(period, amplitude, t_offset)  );
    
    if ((y + offset) == to_mark) {
      return mark();
    }
    else {
      return blank();
    }
  }
}

export function draw_layer_border () {
  return (x, y, width, height, mark) => {
    let is_left = (x == 0);
    let is_right = (x == width - 1);
    let is_top = (y == 0);
    let is_bottom = (y == height - 1);
    let is_corner = ((is_left || is_right) && (is_top || is_bottom));

    if (is_corner) {
      return '*'
    }
    else if (is_left || is_right) {
      return '|';
    }
    else if (is_bottom || is_top) {
      return '―';
    }

    return blank();
  }
}

export default { horizontal_line, vertical_line, sinus_vertical, cosinus_vertical, sinus_horizontal, draw_layer_border };