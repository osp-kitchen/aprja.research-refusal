// rendertable:
// 
// row by row
//
// divide (column) width by cell count
//
// render cell content in width

import { flatten, make_layer, merge } from "./index.js";
import { offset } from "./geometry.js";
import { BlockFilter, fragments_remove_trailing_whitespace, make_fragment } from "./render.js";
import { make_simple_text_frame, TEXT_ALIGN_LEFT } from "./text.js";

const horizontal_spacing = 2;

function render_cell (text, width) {
  return make_simple_text_frame(width - horizontal_spacing, null, text, TEXT_ALIGN_LEFT);
}

function render_row (cells, widths) {
  let x = 0,
      layer = make_layer();

  for (let i = 0; i < cells.length; i++) {
    let cell_width = widths[i],
        cell = offset(x, 0)(render_cell(cells[i], cell_width));
    console.log(cell);
    layer = merge([ layer, cell ]);
    x += cell_width;
  }

  return layer;
}

function extract_texts_and_widths (table) {
  let rows = [],
      widths = [],
      tableRows = table.querySelectorAll('tr');

  for (let r = 0; r < tableRows.length; r++) {
    let cells = tableRows[r].querySelectorAll('td, th'),
        row = [];

    for (let c=0; c < cells.length; c++) {
      let content = cells[c].textContent.trim();
      row.push(content);

      if (widths.length < row.length) {
        widths.push(0);
      }

      widths[c] = Math.max(widths[c], content.length + horizontal_spacing);
    }

    rows.push(row);
  }

  return [ rows, widths ];
}

// Check if this can be more efficient
// Now it renders layers and then shifts
// back to a flat text
function render_table(table, width) {
  console.log(table, width);

  let [ rows, widths ] = extract_texts_and_widths(table),
      total_width = widths.reduce((sum, v) => sum + v, 0);

  if (total_width < width) {
    let grow = width - total_width,
        grow_per_cell = Math.floor(grow / widths.length);

    width = widths.map(v => v + grow_per_cell);
  }
  else if (total_width > width) {
    let width_per_cell = Math.floor(width / widths.length),
        to_shrink = [],
        base_width = 0,
        flexible_width,
        flexible_cell_width;
    
    for (let i=0; i < widths.length; i++) {
      if (widths[i] > width_per_cell) {
        to_shrink.push(i);
      } else {
        base_width += widths[i];
      }
    }

    flexible_width = width - base_width;
    flexible_cell_width = Math.floor(flexible_width / to_shrink.length);

    for (let i=0; i < to_shrink.length; i++) {
      widths[to_shrink[i]] = flexible_cell_width;
    }
  }

  console.log(widths, rows);
  
  let layer = make_layer(),
      y = 0;

  for (let i = 0; i < rows.length; i++) {
    let row = offset(0, y)(render_row(rows[i], widths));
    layer = merge([ layer, row ]);
    y = row.length + 1;
  }

  // let rows = table.querySelectorAll('tr'),
  //     layer = make_layer(),
  //     y = 0;
  // for (let i = 0; i < rows.length; i++) {
  //   let row = offset(0, y)(render_row(rows[i], width));
  //   layer = merge([ layer, row ]);
  //   console.log(render_row(rows[i], width), y);
  //   y = row.length + 1;
  // }

  console.log(flatten(layer));

  return flatten(layer);
}

export const makeTableFilter = (space_before = 0, space_after = 0, geometry) => (
  new BlockFilter('table', (fragments, node, regions) => (
    [ make_fragment((width) => render_table(node, width), geometry) ]
  ), false, space_before, space_after))

export default { makeTableFilter }  