function offset (x, y) {
  return (layer) => (
    Array(y)
      .fill([null])
      .concat(layer.map((row) => Array(x).fill(null).concat(row)))
  );
}

export { offset }