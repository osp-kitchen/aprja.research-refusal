import { renderMarkdown as _renderMarkdown } from './markdown.js';
import { wrap_line } from "./text.js";
import { isCallable } from './isCallable.js';

import { DitherJS } from './ditherjs/ditherjs.js';
import { make_url_api_raw } from './gitlab.js';

export function first_then (first, then)  {
  let called = false;
  return (line) => {
    if (!called) {
      called = true;
      return (isCallable(first)) ? first(line) : first;
    }

    return (isCallable(then)) ? then(line) : then;
  }
}

export function make_geometries (inset=0, shorten=0, prefix='', postfix='', avoid_column_break_inside=null, avoid_column_break_after=null) {
  return [{ 
    inset: inset, 
    shorten: shorten,
    prefix: prefix, 
    postfix: postfix,
    avoid_column_break_inside: avoid_column_break_inside,
    avoid_column_break_after: avoid_column_break_after
  }];
}


export function fragments_extract_texts (fragments) {
  return fragments.map(f => f[0]);
} 

export function flatten_texts (fragments) {
  return fragments_extract_texts(fragments).join('');
}

export function make_fragment (text, geometry) {
  return [ text, geometry ];
}

// Loops through all fragments and adds the provided geometries
export function fragments_add_geometries (fragments, geometries) {
  return fragments.map(f => [ f[0], geometries.concat(f[1]) ]);
}

/**
 * Removes trailing new lines and white spaces on last fragment
 * i.e. removes empty lines at the end of this list of fragments
 * @param {Array} fragments 
 * @returns 
 */
 export function fragments_remove_trailing_whitespace (fragments) {
  while (fragments.length > 0) {
    let last = fragments[fragments.length-1];
    if (!(last[0] || last[0].trim())) {
      // Remove last line if it's empty, or if it's just
      // whitespaces and newlines
      fragments.pop();
    }  else {
      // Line is not empty, break from the loop
      return fragments
    }
  }
  return fragments;
}

export class Filter {
  constructor (selector, textFilter, async = false) {
    this.selector = selector;
    this.textFilter = textFilter;
    this.async = async;
  }
}

export class InlineFilter extends Filter {
  constructor(selector, textFilter, async = false) {
    super(selector, textFilter, async);
  }

  async apply (text, node, regions) {
    return this.textFilter(text, node, regions);
  }
}

export class BlockFilter extends Filter {
  constructor(selector, textFilter, async = false, space_before, space_after) {
    super(selector, textFilter, async);
    this.space_before = space_before;
    this.space_after = space_after;
  }

  async apply(fragments, node, regions) {
    if (this.async) {
      return await this.textFilter(fragments, node, regions);
    }
    else {
      return this.textFilter(fragments, node, regions);
    }
  }
}

export class Registry {
  constructor () {
    this.fragments = [];
  }

  nextKey () {
    return this.fragments.length;
  }

  register (value) {
    this.fragments.push(value);
  }
}


export class Whitespace {
  constructor (height) {
    this.height = height;
  }

  collapse (other) {
    return new Whitespace(Math.max(this.height, other.height));
  }
}


export function parseNode (node, filters, first, last) {
  if (node instanceof Text) {
    let text = node.textContent;

    if (first) {
      text = text.trimLeft();
    }
    
    if (last) {
      text = text.trimRight();
    }
    
    return {
      type: 'text',
      text: node.textContent,
      node: node
    };
  }
  else {
    let token = {
      type: 'block',
      node: node,
      filter: filters.find((filter) => (filter && node.matches(filter.selector))),
      attributes: {},
      tokens: []
    }

    if (!token.filter) {
      token.filter = new BlockFilter('*', (fragments) => fragments);
    }

    for (let a = 0; a < node.attributes.length; a++) {
      token.attributes[node.attributes[a].name] = node.attributes[a].value;
    }

    for (let i = 0; i < node.childNodes.length; i++) {
      token.tokens.push(parseNode(node.childNodes[i], filters, (i==0), ((i+1) == node.childNodes.length)))
    }

    return token;
  }
}

// Flattens the tree recursively
export async function renderNode(node, regions) {
  if ('tokens' in node) {
    let fragments = [];

    for (let i = 0; i < node.tokens.length; i++) {
      fragments = fragments.concat(await renderNode(node.tokens[i], regions));
    }

    if ('filter' in node && node.filter instanceof BlockFilter) {
      // If there is flat text in between, add it.
      let result = await node.filter.apply(fragments, node.node, regions, node.attributes);
      
      // Filter did not return a list of fragments, but a single fragment.
      // Fix inline.
      if (result.length === 2 && typeof result[0] === 'string') {
        console.warn('Filter returned single fragment, rather than list', node.filter, node.node);
        result = [ result ];
      }

      if (node.filter.space_before > 0) {
        result.unshift(new Whitespace(node.filter.space_before));
      }


      if (node.filter.space_after > 0) {
        result.push(new Whitespace(node.filter.space_after));
      }

      return result;
    }
    else if (node.type == 'block' && node.filter instanceof InlineFilter) {
      // Apply an inline filter. Discard geometry and insert default geometry
      
      let textBuffer = '';
      for (let f = 0; f < fragments.length; f++) {
        textBuffer += await node.filter.apply(fragments[f][0], node.node, regions, node.attributes);
      }
      
      return [ make_fragment(textBuffer, make_geometries()) ];
    }
    else {
      return fragments;
    }
  }
  else {
    return [ make_fragment(node.text, make_geometries()) ];
  }
}

export async function render (markdown, filters, frame) {

  let html = _renderMarkdown(markdown),
      domParser = new DOMParser(),
      doc = domParser.parseFromString(html, 'text/html'),
      parsed = parseNode(doc.body, filters);

  console.log(doc, parsed)

  let result = await renderNode(parsed, regions);

  let body = frame(result); //make_column_fragments(result, 50, make_geometries((l) => Math.round(sharkteeth(20, 20)(l)), (l) => Math.round(3 - sinus(20, 3, 0)(l))));

  let pre = document.body.appendChild(document.createElement('pre'));
  // pre.appendChild(document.createTextNode(body.join('\n')));
}

function drop_empty_tokens (parsed) {
  // For now explicitly non recursive!
  parsed.tokens = parsed.tokens.filter((token) => (token.type !== 'text' || token.text.trim() !== ''));

  return parsed;
}

export function renderImages (pre, regions) {
  let patt = new RegExp(/{IMG:(\d+):(\d+):(\d+):(true|false)}/, 'g'),
      searchText = pre.textContent,
      result;

  while ((result = patt.exec(searchText)) !== null) {
    let image_index = result[1],
        width = parseInt(result[2]),
        height = parseInt(result[3]),
        apply_dither = (result[4] == 'true') ? true : false,
        string_index = result['index'];
  
    let range = new Range();
    range.setStart(pre.firstChild, string_index);
    range.setEnd(pre.firstChild, string_index + 1);
  
    let rect = range.getBoundingClientRect();

    // Select one glyph to get the size of a char
    console.log(rect, width, height);

    let img_width = width * rect.width,
        img_height = height * 15, // rect.width,
        x = rect.x - pre.parentNode.getBoundingClientRect().x,
        y = rect.y - pre.parentNode.getBoundingClientRect().y;


    range.setEnd(pre.firstChild, string_index + 80);

    // let downloadedImg = new Image;
    // downloadedImg.crossOrigin = "no-cors";
    // downloadedImg.addEventListener("load", () => { console.log('hello!') }, false);
    // downloadedImg.src = regions['images'].fragments[parseInt(image_index)];
    let img = document.createElement('img');
    pre.parentNode.appendChild(img);
    img.style.position = 'absolute';
    img.style.left = x + 'px';
    img.style.top = y + 'px';
    img.style.width = img_width + 'px';
    img.style.height = img_height + 'px';
    img.crossOrigin = 'anonymous';
    
    if (apply_dither) {
      img.addEventListener('load', function () {
        let ditherjs = new DitherJS({
          "step": 4, // The step for the pixel quantization n = 1,2,3...
          "palette": [[0, 0, 0], [255, 255, 255]], // an array of colors as rgb arrays
          "algorithm": "atkinson" // one of ["ordered", "diffusion", "atkinson"]
        });
        ditherjs.dither(img);
      });
    }
    
    img.src = regions['images'].fragments[parseInt(image_index)];
  }
};

export const makeImageLayer = async(src, width, apply_dithering, regions) => (
  new Promise((resolve, reject) => {
    // Construct and load the image
    // resolve with url and dimensions for now
    const img = new Image();
    
    img.addEventListener('load', () => {
      // Returns a text at height, relevant to the context
      // compensate for non-squeare 'pixels' in text: each
      // glyph/character is higher than it's wide
      // In chrome, height: 15.5px, width: 6.21875
      let scale = (width / img.width) / 2.4925,
          height = Math.ceil(img.height * scale);
      
      let key = regions['images'].nextKey();

      regions['images'].register(img.getAttribute('src'));

      let imgCode = `{IMG:${ key }:${ width }:${ height }:${ (apply_dithering) ? 'true' : 'false' }}`;
      resolve([ imgCode.split('') ]);
    });

    img.src = src;
  })
)

/**
 * Function that makes a block filter. As it is async the filter returns a promise.
 * 
 * Promise in itselve makes a fragment. Fragment itself is callable as the width
 * and height of the image are based on the width of the textbox.
 */
export const makeImageFilter = (selector, space_before = 1, space_after = 1, apply_dithering, base_url) => new BlockFilter(selector, async (text, node, regions) => (
  new Promise((resolve, reject) => {
    // Construct and load the image
    // resolve with url and dimensions for now
    const img = new Image();
    
    img.addEventListener('load', () => {
      resolve([ make_fragment(
        (width) => {
          // Returns a text at height, relevant to the context
          // compensate for non-squeare 'pixels' in text: each
          // glyph/character is higher than it's wide
          let scale = (width / img.width) / 2.4522,
              height = Math.ceil(img.height * scale);
          
          let key = regions['images'].nextKey();

          // console.log(scale, width, img.width);

          regions['images'].register(img.getAttribute('src'));

          return `{IMG:${ key }:${ width }:${ height }:${ (apply_dithering) ? 'true' : 'false' }}` + Array(height).fill('\n').join('')
        }, make_geometries(0,0,'','', true, null)
      ) ]);
      
    });
    
    let src = node.getAttribute('src');

    if (!src.startsWith('http')) {
      if (base_url.endsWith('/')) {
        base_url = base_url.substr(0, base_url.length - 1);
      }

      let encoded_file_path = base_url + '/' + src;
      
      src = make_url_api_raw(encoded_file_path);
    }

    img.src = src;
  })), true, space_before, space_after)


export const makeDoubledHeaderFilter = (selector, space_before = 0, space_after = 0, uppercase=false) => new BlockFilter(selector, (fragments, node, regions) => (
  [make_fragment((width) => {
    let chars_per_line = Math.floor(width / 3),
        lines = [],
        line,
        remaining = flatten_texts(fragments);

    if (uppercase) {
      remaining = remaining.toUpperCase();
    }

    while (remaining) {
      [ line, remaining ] = wrap_line(remaining, chars_per_line);

      let doubled_line = '';

      for (var c = 0; c < line.length; c++) {
        doubled_line += line[c] + line[c] + ' ';
      }

      lines.push(doubled_line);
      lines.push(doubled_line);
      lines.push('');
    }

    return lines.join('\n')
  }, make_geometries(0, 0, '',  '', true, null))]
), false, space_before, space_after);

export const makeUnderlineFilter = (selector, underline_chars='-', space_before = 0, space_after = 0, geometry=null, filter = (filter_text) => filter_text) => new BlockFilter(selector, (fragments, node, regions) => (
  [make_fragment((width) => {
    let lines = [],
        line,
        remaining = filter(flatten_texts(fragments));

    while (remaining) {
      console.log('Wrapping..', remaining, line);
      [ line, remaining ] = wrap_line(remaining, width);

      // Construct an underline with same length as the text line
      let underline = Array(Math.ceil(line.length / underline_chars.length))
          .fill(underline_chars)
          .join('')
          .slice(0, line.length);

      lines.push(line);
      lines.push(underline);
    }

    return lines.join('\n')
  }, geometry)]
), false, space_before, space_after);

/**
 * Renders a string of markdown to a fragment list. Which can
 * later be used to be set in a text frame.
 * 
 * @param {strin} markdown Markdown to render into fragments
 * @param {Array} filters List of filters to apply
 * @param {Dict} regions Dictionary witht the region registries
 * @returns FragmentList
 */
export async function renderMarkdown (markdown, filters, regions) {
  // console.log(filters);
  // filters = filters.concat([
  //   ImageFilter('img', 1, 1)
  // ])

  let html = _renderMarkdown(markdown),
      domParser = new DOMParser(),
      doc = domParser.parseFromString(html, 'text/html');

  // 'Unwrap' images which are the only child of a paragraph
  let images = doc.querySelectorAll('p img');

  console.log(images, doc);

  for (let i=0; i < images.length; i++) {
    images[i].parentNode.dataset.hadImages = true;
    images[i].parentNode.parentNode.insertBefore(images[i], images[i].parentNode);
  }

  let emptyParagraphs = doc.querySelectorAll('p[data-had-images]:empty');

  for (let i = (emptyParagraphs.length - 1); i >= 0; i--) {
    emptyParagraphs[i].remove();
  }

  console.log(doc);

  // Make a 'simplified' structure from the html tree
  let parsed = parseNode(doc.body, filters);

  console.log(parsed);

  // Dropping tokens with just whitespace on the first level.
  // Probably have to make this recursive?
  parsed = drop_empty_tokens(parsed);


  return await renderNode(parsed, regions);
}

export default { make_geometries, make_fragment, fragments_extract_texts, fragments_add_geometries, fragments_remove_trailing_whitespace, fragments_remove_trailing_whitespace, InlineFilter, BlockFilter, makeImageFilter, makeImageLayer, makeDoubledHeaderFilter, makeUnderlineFilter, Registry, parseNode, renderNode, render, renderImages, renderMarkdown }

// (async function () {
//   let markdown = await asciiWriter.getMarkdown();
//   render(markdown, value_filters);
// });

// Flow into
// regions[name].register(fragment)
// 
// Get next fragment key <name>
// regions[name].getNextKey()
//
// Get next fragment number
// (regions[name].getNextKey() + 1)
//
// Flow from <name>
// regions[name].fragments

// asciiWriter.render.fragments_add_geometries()