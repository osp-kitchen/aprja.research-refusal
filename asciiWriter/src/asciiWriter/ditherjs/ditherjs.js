/**
* Javascript dithering library
* @author 2014 Daniele Piccone
* @author www.danielepiccone.com
* */

import utils from './utils.js';

export const DitherJS = function DitherJS (options) {

    this.options = options || {};
    this.options.algorithm = this.options.algorithm || 'ordered';
    this.options.step = this.options.step || 1;
    this.options.className = this.options.className || 'dither';
    this.options.palette = this.options.palette || utils.defaultPalette;

};

import { orderedDither } from './algorithms/orderedDither.js';
import { errorDiffusionDither } from './algorithms/errorDiffusionDither.js';
import { atkinsonDither } from './algorithms/atkinsonDither.js';

DitherJS.orderedDither = orderedDither;
DitherJS.atkinsonDither = atkinsonDither;
DitherJS.errorDiffusionDither = errorDiffusionDither;

DitherJS.prototype.ditherImageData = function ditherImageData(imageData, options) {
    options = options || this.options;

    var ditherFn;
    if (options.algorithm == 'diffusion')
        ditherFn = DitherJS.errorDiffusionDither;
    else if (options.algorithm == 'ordered')
        ditherFn = DitherJS.orderedDither;
    else if (options.algorithm == 'atkinson')
        ditherFn = DitherJS.atkinsonDither;
    else
        throw utils.errors.InvalidAlgorithm;

    var startTime;
    if (options.debug) {
        startTime = Date.now();
    }

    var output = ditherFn.call(
        this,
        imageData.data,
        options.palette,
        options.step,
        imageData.height,
        imageData.width
    );

    if (options.debug) {
        console.log('Microtime: ', Date.now() - startTime);
    }

    imageData.data.set(output);
};

DitherJS.prototype.colorDistance = function colorDistance(a, b) {
    return Math.sqrt(
        Math.pow( ((a[0]) - (b[0])),2 ) +
        Math.pow( ((a[1]) - (b[1])),2 ) +
        Math.pow( ((a[2]) - (b[2])),2 )
    );
};

DitherJS.prototype.approximateColor = function approximateColor(color, palette) {
    var findIndex = function(fun, arg, list, min) {
        if (list.length == 2) {
            if (fun(arg,min) <= fun(arg,list[1])) {
                return min;
            }else {
                return list[1];
            }
        } else {
            var tl = list.slice(1);
            if (fun(arg,min) <= fun(arg,list[1])) {
                min = min;
            } else {
                min = list[1];
            }
            return findIndex(fun,arg,tl,min);
        }
    };
    var foundColor = findIndex(this.colorDistance, color, palette, palette[0]);
    return foundColor;
};

DitherJS.prototype.dither = function dither(target, options) {

    if (typeof target === 'string') {
        this._fromSelector(target, options);
    }

    if (
        typeof target === 'object' &&
        target instanceof window.HTMLImageElement
    ) {
        this._fromImgElement(target, options);
    }

};

DitherJS.prototype._replaceElementWithCtx = function(el) {
    var ctx;
    var canvas = document.createElement('canvas');

    // Over scaling to have better resolution for print.
    canvas.height = el.clientHeight * 8;
    canvas.width = el.clientWidth * 8;

    canvas.style.position = el.style.position;
    canvas.style.left = el.style.left;
    canvas.style.top = el.style.top;
    canvas.style.width = el.style.width;
    canvas.style.height = el.style.height;

    el.parentNode.replaceChild(canvas, el);

    canvas.className = el.className;

    if (el.style.visibility === 'hidden') {
        canvas.style.visibility = 'visible';
    }

    ctx = canvas.getContext('2d');
    ctx.imageSmoothingEnabled = false;

    return ctx;
};

DitherJS.prototype._fromImgElement = function(el, options) {

    var h = el.clientHeight * 8;
    var w = el.clientWidth * 8;

    var ctx = this._replaceElementWithCtx(el);

    ctx.drawImage(el,0,0,w,h);

    var imageData = ctx.getImageData(0,0,w,h);

    this.ditherImageData(imageData, options);

    ctx.putImageData(imageData,0,0);
};

DitherJS.prototype._fromSelector = function(selector, options) {
    var that = this;
    var elements = document.querySelectorAll(selector);
    var handleOnLoad = function(el) {
        return function () {
            that._fromImgElement(el, options);
        };
    };

    try {
        for (var i=0; i<elements.length; i++) {
            var el = elements[i];
            el.style.visibility = 'hidden';
            el.src = el.src + '?' + Date.now();
            el.onload = handleOnLoad(el);
        }
    } catch (e) {
        console.error(e);
    }
};

export default DitherJS;
