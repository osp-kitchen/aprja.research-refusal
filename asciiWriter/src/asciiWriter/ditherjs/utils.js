export const defaultPalette = [
    [0, 0, 0],
    [255, 0, 255],
    [0, 255, 255],
    [255, 255, 255]
];

export const errors = {
    CanvasNotPresent: new Error('CanvasNotPresent'),
    TargetNotBuffer: new Error('TargetNotBuffer'),
    InvalidAlgorithm: new Error('InvalidAlgorithm')
};

export const targetTypes = {
    selector: 'SELECTOR',
    buffer: 'BUFFER'
};

export const Canvas = null;

// try {
//     exports.Canvas = require("canvas");
// } catch (e) {
//     exports.Canvas = null;
// }

export default { defaultPalette, errors, targetTypes, Canvas }