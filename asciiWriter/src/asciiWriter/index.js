import * as text from './text.js'
import * as marks from './marks.js';
import * as patterns from './patterns.js';
import * as textEffects from './textEffects.js';
import * as geometry from './geometry.js';
import * as render from './render.js';
import * as patternKernels from './patternKernels.js';
import * as markdown from './markdown.js';
import * as gitlab from './gitlab.js';
import * as table from './table.js'


const SPACE_CHAR = ' ';

export  function make_layer (width, height, fillChar=null) {
  let layer =  [];
  for (let i=0; i < height; i++) {
    layer.push(Array(width).fill(fillChar))
  }
  return layer;
}

export function visit (layer, pattern, mark) {
  const height = layer.length,
        width = layer[0].length;

  return layer.map((row, y) => row.map((cell, x) => (
    pattern(x, y, width, height, mark)
  )));
}

export function merge (layers) {
  // Find biggest layer height
  // Find biggesy layer width (looking at first row)
  const height = Math.max.apply(this, layers.map(l => l.length)),
        width = Math.max.apply(this, layers.map(l => Math.max.apply(this, l.map(r => r.length)))),
        base = make_layer(width, height);

  layers.forEach(l => {
    l.forEach((row, y) => {
      row.forEach((char, x) => {
        if (char !== null) {
          base[y][x] = char;
        }
      })
    });
  });

  return base;
}

export function flatten (layer) {
  return layer.map((row) => row.map((c) => (c === null) ? SPACE_CHAR : c).join('')).join('\n');
}

export function printInElement(layer, element) {
  element.appendChild(document.createTextNode(flatten(layer)));
}

export default { text, marks, patterns, textEffects, geometry, render, patternKernels, markdown, gitlab, table, make_layer, visit, merge, printInElement };