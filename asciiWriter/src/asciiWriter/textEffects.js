function space (spaceChar=' ') {
  return (text) => (
    text.split('').join(spaceChar)
  )
}

function double (text) {
  return text.split('').reduce((out, char) => out + char + char, ' ')
}

function replace (search, replace) {
  return (text) => text.replace(search, replace)
}

function uppercase (text) {
  return text.toUpperCase();
}

function lowercase (text) {
  return text.toLowerCase();
}

export { space, double, replace, uppercase, lowercase }