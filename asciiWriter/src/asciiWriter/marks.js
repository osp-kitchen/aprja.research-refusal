function choice (options) {
  return options[Math.round(Math.random() * options.length)]
}

function random (marks) {
  return () => choice(marks)
}

function text (chars, loop) {
  chars = chars.split('')
  return () => {
    let char = chars.shift()

    if (loop) {
      chars.push(char)
    }

    return char
  }
}

function single (char) {
  return () => char
}

function blank () {
  return null;
}

export { choice, random, text, single, blank };