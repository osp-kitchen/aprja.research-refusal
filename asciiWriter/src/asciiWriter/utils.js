function make_layer (width, height, fillChar = null) {
  let layer =  [];
  for (let i=0; i < height; i++) {
    layer.push([].fill(fillChar, 0, width))
  }
  return layer;
}

function visit (layer, pattern, mark) {
  const height = layer.length,
        width = layer[0].length;

  return layer.map((row, y) => row.map((cell, x) => {
    pattern(x, y, width, height, mark)
  }));
}

function merge (layers) {
  // Find biggest layer height
  // Find biggesy layer width (looking at first row)
  const height = Math.max.apply(this, layers.map(l => l.length)),
        width = Math.max.apply(this, layers.map(l => l[0].length)),
        base = make_layer(width, height);

  layers.forEach(l => {
    l.forEach((row, y) => {
      row.forEach((char, x) => {
        if (char !== null) {
          base[y][x] = char;
        }
      })
    });
  });
}

export { make_layer, visit, merge };
