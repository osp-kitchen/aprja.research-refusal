export const GITLAB_PROJECT_ID='23857158';
export const GITLAB_API_URL='https://gitlab.com';

export const make_url_api_raw = (path) => (
  `${ GITLAB_API_URL }/api/v4/projects/${ GITLAB_PROJECT_ID }/repository/files/${ encodeURIComponent(path) }/raw`
)

export default { make_url_api_raw };