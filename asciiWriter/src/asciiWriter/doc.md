Line
# List of strings with each a length of one
Array<string<0>, ... > 

Layer
# Flattened content. A list of a list of chars.
Array<Line>

Geometry 
# geometry holds information on 
# 
{
  inset: int|function|null
  shorten: int|function|null
  prefix: string|function|null
  postfix: string|function|null
  avoid_column_break_inside: Boolean
  avoid_column_break_after: Boolean
}

Fragment
# Fragments are part of markdown conversion.
# Frgaments are pieces of content, or a callable with geometry attached
[ string|function, Geometry]

Whitespace
# Represents whitespace
# the height is defined measured in lines.
# Two subsequent whitespaces can be 'collapsed' where the 
# height of the hightes of the two is taken.

Line
# [ [ str:char, ...], bool:nonbreakingblock?, bool:avoid_break_after?, bool:last_line_of_fragment? ]]