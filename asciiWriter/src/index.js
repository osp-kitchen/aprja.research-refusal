/**
 * @license
 * 
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Example of including Blockly with using Webpack with 
 *               defaults: (English lang & JavaScript generator).
 * @author samelh@google.com (Sam El-Husseini)
 */

import * as Blockly from 'blockly';
import './blockly/ascii-writer/blocks';
import './blockly/ascii-writer/generators';
import '@blockly/block-plus-minus';

import { get, get_text, post } from './api.js';

function asDataUrl(content, mimeType) {
  return `data:${mimeType};charset=utf-8,${encodeURIComponent(content)}`;
}

function triggerDownload(content, name, mime) {
  const a = document.createElement('a');
  a.setAttribute('href', asDataUrl(content, mime));
  a.setAttribute('download', name);

  if (document.createEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    a.dispatchEvent(event);
  }
  else {
    a.click();
  }
}

function openFile(callback) {
  var input = document.createElement('input')
  input.setAttribute('type', 'file');
  input.addEventListener('change', function (e) {
    const reader = new FileReader();
    const name = input.files[0].name;

    reader.onload = (e) => {
      callback(e.target.result, name);
    }

    reader.readAsText(input.files[0]);
  }.bind(this));

  if (document.createEvent) {
    const event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    input.dispatchEvent(event);
  }
  else {
    input.click();
  }
}

function loadBlocks(workspace) { // xml is the same block xml you stored
  openFile(function (xml, filename) {
    window.blocklyEditorFileName = filename;
    loadBlocksFromRawXML(xml, workspace);
  });
}

function backupBlocks(workspace) {
  // <xml xmlns="https://developers.google.com/blockly/xml"></xml>
  var xmlDom = Blockly.Xml.workspaceToDom(workspace);

  if (xmlDom.childElementCount > 0) {
    // Do not backup empty sketch
    var xmlText = Blockly.Xml.domToPrettyText(xmlDom);
    // do whatever you want to this xml
    if ('localStorage' in window) {
      window.localStorage.setItem('blocklyBackup__block', xmlText);
      window.localStorage.setItem('blocklyBackup__filename', window.blocklyEditorFileName);
    }
  }
}

function restoreBlocksFromBackup(workspace) {
  if ('localStorage' in window) {
    const xml = window.localStorage.getItem('blocklyBackup__block');
    window.blocklyEditorFileName = window.localStorage.getItem('blocklyBackup__filename');
    loadBlocksFromRawXML(xml, workspace);
  }
}

function loadBlocksFromRawXML(xml, workspace) {
  if (typeof xml != "string" || xml.length < 5) {
    return false;
  }
  try {
    var dom = Blockly.Xml.textToDom(xml);
    workspace.clear();
    Blockly.Xml.domToWorkspace(dom, workspace);
    return true;
  } catch (e) {
    return false;
  }
}

function exportBlocksToXML(workspace) {
  var xmlDom = Blockly.Xml.workspaceToDom(workspace);
  var xmlText = Blockly.Xml.domToPrettyText(xmlDom);

  return xmlText
}

window.blocklyEditorFileName = 'aprja-asciiWriter-spread.xml';

document.addEventListener("DOMContentLoaded", function () {
  if (!window.blocklyEditor) {
    window.blocklyEditor = {};
  }

  const workspace = window.blocklyEditor.workspace = Blockly.inject('blockly--div',
    {
      toolbox: document.getElementById('toolbox'),
      media: 'media/',
      zoom: {
        controls: true,
        wheel: true,
        startScale: 1.0,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2,
        pinch: true
      },
      grid: {
       spacing: 20,
       length: 3,
       colour: '#ccc',
       snap: true
      }
    });

  const js = 'JavaScript';

  const generatePreview = () => {
    const jsCode = Blockly[js].workspaceToCode(workspace);
    const area = document.querySelector('#blockly--code textarea');
    area.value = jsCode;
    
    document.getElementById('blockly--preview').contentWindow.postMessage({
      type: 'asciiWriter::preview',
      code: jsCode
    });
  }

  const exportSketch = () => {
    window.blocklyEditorFileName = window.prompt('Filename', window.blocklyEditorFileName);
    if (!window.blocklyEditorFileName.endsWith('.xml')) {
      window.blocklyEditorFileName += '.xml'
    }

    triggerDownload(exportBlocksToXML(workspace), window.blocklyEditorFileName);
  }

  const importSketch = () => {
    var xmlDom = Blockly.Xml.workspaceToDom(workspace);

    if (xmlDom.childElementCount == 0
      || window.confirm("Continue? Loading a sketch will overwrite any unsaved changes.")) {
      backupBlocks(workspace);
      loadBlocks(workspace);
    }
  }

  const restoreSketch = () => {
    var xmlDom = Blockly.Xml.workspaceToDom(workspace);

    if (xmlDom.childElementCount == 0
      || window.confirm("Restoring the backup will overwrite the current sketch. Continue?")) {
      restoreBlocksFromBackup(workspace);
    }
  }

  const previewButton = document.getElementById('button--preview');
  previewButton.addEventListener('click', generatePreview);

  const exportButton = document.getElementById('button--export');
  exportButton.addEventListener('click', exportSketch);

  const importButton = document.getElementById('button--import');
  importButton.addEventListener('click', importSketch);

  const restoreButton = document.getElementById('button--restore');
  restoreButton.addEventListener('click', restoreSketch);

  let panels = document.querySelectorAll('[data-panel]');

  for (let p = 0; p < panels.length; p++) {
    panels[p].addEventListener('toggle', function () {
      Blockly.svgResize(workspace);
    });
  }

  window.addEventListener('unload', function () {
    backupBlocks(workspace);
  });


  // Investigate how to correct for local timezones
  function toDateTimeString (d) {
    if (d && d instanceof Date) {
      return `${ d.getFullYear() }-${ d.getMonth() + 1 }-${ d.getDate() } ${ d.getHours() }:${ d.getMinutes() } CET`;
    }

    return null;
  }

  const state = {
    spreads: {
      spreads: null, // nulll | Array<>
      loading: false
    },
    versions: {
      versions: null, // null | Array<>
      loading: false
    },
    version: {
      version: null,  // null | xml
      loading: false,
      saving: false,
    },
    selectedSpread: null,
    selectedSpreadName: null,
    selectedVersion: null
  }

  const selectSpread = document.querySelector('#select--spread');
  const selectVersion = document.querySelector('#select--version');
  const butonOpenDialog = document.querySelector('#button--open-dialog');
  const buttonOpen = document.querySelector('#button--server--open');
  const buttonSave = document.querySelector('#button--server--save');

  function loadSpreads() {
    if (state.spreads.loading === false) {
      state.spreads.loading = true;
      state.spreads.spreads = null;

      while (selectSpread.lastChild) {
        selectSpread.removeChild(selectedSpread.lastChild);
      }

      get('spreads')
        .then((spreads) => {
          state.spreads.loading = false;
          state.spreads.spreads = spreads;
          spreads.forEach(([id, name, page_numbers ]) => {
            let option = document.createElement('option');
            option.setAttribute('value', id);
            option.appendChild(document.createTextNode(`${ name } (${ page_numbers[0] }-${ page_numbers[1] })`));
            selectSpread.appendChild(option);
          });
          state.selectedSpread = parseInt(selectSpread.value);
          loadVersions();
        })
        .catch(() => {
          state.spreads.loading = false;
        })
    }
  }
  
  function loadVersions() {
    if (state.versions.loading === false) {
      state.versions.loading = true;
      state.versions.versions = null;

      while (selectVersion.lastChild) {
        selectVersion.removeChild(selectVersion.lastChild);
      }

      get(`spread/${ state.selectedSpread }`)
        .then((versions) => {
          state.versions.loading = false;
          state.versions.versions = versions;
          // [ (int:id, int:timestamp), ... ]
          versions.forEach(([id, timestamp ], k) => {
            let option = document.createElement('option');
            let date = new Date(timestamp * 1000); // Server timestamp is in seconds. Javascript uses milliseconds
            let label = `${ id + 1 }: ${ toDateTimeString(date) }`;

            if (k == 0) {
              label = '(latest) ' + label;
            }

            option.setAttribute('value', id);
            option.appendChild(document.createTextNode(label));
            selectVersion.appendChild(option);
          });
          state.selectedVersion = parseInt(selectVersion.value);
        })
        .catch(() => {
          state.spreads.loading = false;
        })
    }
  }
  
  function loadVersion() {
    // @app.route('/spread/<int:spread_id>/<int:version>')
    if (state.version.loading === false
        && state.selectedSpread > -1
        && state.selectedVersion > -1
    ) {
    var xmlDom = Blockly.Xml.workspaceToDom(window.blocklyEditor.workspace);

    if (xmlDom.childElementCount == 0
        || window.confirm("Continue? Loading a different version will overwrite any unsaved changes.")
    ) {
      state.version.loading = true;
      state.version.version = null;
      showDialog('loading');
      get_text(`spread/${ state.selectedSpread }/${ state.selectedVersion }`)
        .then((rawXML) => {
          state.version.loading = false;
          state.version.version = rawXML;
          state.selectedSpreadName = state.spreads.spreads.find(([id, _, __]) => id == state.selectedSpread)[1];
          updateSpreadName();
          loadBlocksFromRawXML(rawXML, window.blocklyEditor.workspace);
          closeDialog('loading');
          generatePreview();
        })
        .catch(() => {
          state.version.loading = false;
          closeDialog('loading');
          showDialog('loading-error');
        });
      }
    }
  }

  function updateSpreadName () {
    window.blocklyEditorFileName = `aprja-${state.selectedSpreadName}.xml`;
    document.querySelector('#selected-spread--name').textContent = state.selectedSpreadName;
  }
  
  function store() {
    // @app.route('/store', methods=["POST"])
    // spread_id = request.forms['spread_id']
    // version_xml = request.forms['version']

    if (state.version.saving === false
        && state.selectedSpread > -1
    ) {
      showDialog('saving');
      let xml = exportBlocksToXML(window.blocklyEditor.workspace);
      post('store', {
        spread_id: state.selectedSpread,
        version: xml
      })
      .then(function () {
        state.version.saving = false;
        closeDialog('saving');
        loadVersions();
      })
      .catch(function () {
        state.version.saving = false;
        closeDialog('saving');
        showDialog('save-error');
      })
    }
  }

  function showDialog(name) {
    let dialog = document.querySelector(`[data-dialog=${ name }]`)

    if (dialog) {
      dialog.dataset.active = true;
    }
  }

  function closeDialog(name) {
    let dialog = document.querySelector(`[data-dialog=${ name }]`)

    if (dialog) {
      delete dialog.dataset.active;
    }
  }

  selectSpread.addEventListener('change', () => {
    state.selectedSpread = parseInt(selectSpread.value);
    loadVersions();
  });

  selectVersion.addEventListener('change', () => {
    state.selectedVersion = parseInt(selectVersion.value);
  });

  butonOpenDialog.addEventListener('click', function () {
    showDialog('open');
  })

  buttonOpen.addEventListener('click', function () {
    loadVersion();
    closeDialog('open');
  });

  buttonSave.addEventListener('click', store);

  loadSpreads();
  
  var dialogCloseButtons = document.querySelectorAll('[data-dialog-close]');

  for (var i=0; i < dialogCloseButtons.length; i++) {
    dialogCloseButtons[i].addEventListener('click', function () {
      closeDialog(this.parentElement.parentElement.dataset.dialog);
    });
  }
});


// function retreiveMarkdownFromGitlabAPI('https://gitlab.com', 23857158, 'not working group/bodytext.md')
//   .then(function (markdown) {
//     console.log(markdown);
//   });