### References: 
- Chua, C. 2020. “Post-Imperial Oceanics | Charmaine Chua.” https://youtu.be/2Xz0PXtNUpE
 
- Danyluk, M. 2017. “Capital’s Logistical Fix: Accumulation, globalization, and the survival of capitalism.” *Environment and Planning D: Society and Space*, 36(4), 630-647.
 
- Khalili, L. 2020. “Behind the Beirut Explosion Lies the Lawless World of International Shipping.” *The Guardian*, August 8. https://www.theguardian.com/commentisfree/2020/aug/08/beirut-explosion-lawless-world-international-shipping-  
 
- Mezzadra, S., & Neilson, B. 2013. *Border as method, or, The Multiplication of Labor*. Durham: Duke University Press.

- Newman, B. 2007. “A Bitter Harvest: Farmer suicide and the unforeseen social, environmental and economic impacts of the Green Revolution in Punjab, India.” Institute for Food and Development Policy, Development Report No. 15, 1-34.

- Sandhu, A. 2021. "Left, Khaps, Gender, Caste: The solidarities propping up the farmers’ protest." *The Caravan*, 13 January. https://caravanmagazine.in/agriculture/left-punjab-haryana-caste-gender-solidarities-farmers-protest

- Sen, A. & Ghosh, J. 2017. “Indian Agriculture after Liberalisation.” Bangladesh Development Studies, Vol. XXXX, A, 53-71.

- Tsing, A. L. 2012. “On Nonscalability: The Living World Is Not Amenable to Precision-Nested Scales.” *Common Knowledge*, 18(3), 505-524.

- Ziadah, R. 2018. “Transport Infrastructure & Logistics in the Making of Dubai Inc.” *International Journal of Urban and Regional Research*, 182-197.