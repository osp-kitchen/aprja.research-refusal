# EDITORIAL - RESEARCH REFUSAL

Writing in 1965, Mario Tronti’s claim was that the greatest power of the working class is refusal: the refusal of work, the refusal of capitalist development, and the refusal to bargain within a capitalist framework. One can see how this "strategy of refusal" has been utilised in all sorts of instances by social movements, but how does this play out now in the context of wider struggles over autonomy today – not just in terms of labour power and class struggles; but also intersectional feminism and queer politics; race and decolonialism, geopolitics, populism, environmental concerns; and the current pandemic? In what ways does a refusal of production manifest itself in contemporary artistic, political, social, cultural, or other movements? And, how might a refusal of certain forms of production come together with a politics of care and "social closeness"? The transmediale festival announcement puts it this way: From the small acts of refusal that reside in the mundane and everyday, to tender forms of resistance that allow us to repair collective infrastructure, transmediale 2021–22 will map out the political agency of refusal, examining its potential to form new socio-political realities grounded in care, hope, and desire." 

This newspaper presents the outcome of an online workshop (organized by Digital Aesthetics Research Centre, Aarhus University; Centre for the Study of the Networked Image, London South Bank University; and transmediale festival, Berlin) with the participation of nine different groups located at different geographical locations, some inside and some outside the academy. Each group was selected on the basis of an open call and has taken part in a shared mailing list, creating a common list of references, and discussing strategies of refusal, and how these might relate to practices of research and its infrastructures: What might be refused, and in what ways; how might academic autonomy be preserved in the context of capitalist tech development, especially perhaps in the present context of online delivery and the need for alternatives to corporate platforms (e.g. Zoom, Teams, Skype, and the like); and how to refuse research itself, in its instrumental form?

Following the workshop, each group has been asked to produce a section of this newspaper that in different ways represents the group’s abstractions on the subject. The design has been developed by Open Source Publishing, a collective renowned for a practice that questions the influence and affordance of digital tools in graphic design, and who works exclusively with free and open source software. The intention behind this publication has, in this way, been to explore the expanded possibilities of acting, sharing, and making, differently – beyond the normative production of research and its dissemination. Importantly, it has also been a means to allow emerging researchers to present their ideas to the wider community of the transmediale festival in an accessible form. The newspaper is distributed at the festival’s various physical events in Berlin, as well as being available for download as part of the festival's electronic Almanac  ([https://transmediale.de/almanac](https://transmediale.de/almanac)) and at the publisher’s website ([http://darc.au.dk](http://darc.au.dk)). Following the workshop and publication of this newspaper, all participants have been invited to submit extended versions of their research to the online journal, _A Peer-Reviewed Journal About Research Refusal_ ([http://aprja.net](http://aprja.net)), published Summer 2021.

Christian Ulrik Andersen & Geoff Cox

_Peer-reviewed Newspaper_, Volume 10, Issue 1, 2021.

Edited by all authors

Published by Digital Aesthetics Research Center, Aarhus University, in collaboration with Centre for the Study of the Networked Image, London South Bank University, and transmediale, Berlin

Design: Gijs de Heij, Open Source Publishing, [http://osp.kitchen/](http://osp.kitchen/)

CC Licence: [Attribution-Noncommercial-Sharealike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)

ISSN (PRINT): 2245-7593

ISSN (PDF): 2245-7607
