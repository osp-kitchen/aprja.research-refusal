🔥HOT🔥TAKE🔥MACHINE 🔥

| | | | | | | | |
| --- | --- | --- | --- | --- | --- | --- | --- |
| | first letter of your first name | | your date of birth | | first letter of your last name | | your star sign |
| A | refusing | 1-2 | refusal | A | as a site of resistance | aries | towards speculative practices |
| B | re-producing | 3-4 | virtual infrastructures | B | as failing compatibilities | taurus | in societies after globalization | compatibilities
| C | acknowledging | 5-6 | our present digital culture | C | outlining gestures of knowledge-production | gemini | within activism and politics | 
| D | reclaiming | 7-8 | the possibility of emergence | D | - mobilising counter-cultural dreaming | cancer | in emerging machinic ecologies |
| E | re-imagining | 9-10 | the legacies of critical net culture | E | as a radical mulitplicity | leo | in post-socialist utopias  |
| F | destabilizing | 11-12 | political imaginaries | F | as embodied agency | virgo | against ubiquitous networking |
| G | mapping | 12-13 | speculative thinking | G | as modes of cultural | libra | opening up rich spaces of cultural negotiation |
| H | deconstructing | 14-15 | techno-utopias | H | as new forms of care and solidarity | scorpio | in today&#39;s algorithmic filterings |
| I | exploring | 16-17 | the multiplicity | I | - a textual experiment | sagittarius | for futuristic high-tech |
| J | contesting | 18-19 | the desire for digital emancipation | J | - a radical social architecture | capricorn | between trash and treasure |
| K | interrogating | 20-21 | the densification of information | K | to promote auto-criticality | aquarius | beyond a marxist transhumanism |
| L | investigating | 22-23 | new vocabularies and sensibilities | L | - possible new ways of resisting | pisces | against the anxieties of late capitalism critique |
| M | articulating | 24-25 | the productions of crisis | M | as a mode of relation | | |
| N | re-examining | 26-27 | human spaces of mediation and agency | N | as artistic intervention | | |
| O | unlearning | 28-29 | the dark sides of network culture | O | - e-waste dumps as starting point | | |
| P | re-tracing | 30-31 | digital stimulation | P | as temporal superimposition | | |
| Q | emphasizing | | | Q | of our present digital culture | | |  
| R | unveiling | | | R | using digital technologies | | |
| S | reproducing | | | S | - artistic sense-making and speculative scenarios | | |
| T | decentering | | | T | - a self-reflective collaboration | | |
| U | re-envisioning | | | U | as paradoxical nostalgia | | |
| V | unarchiving | | | V | - decolonial projections | | |
| W | evoking | | | W | in an intersectional framework | | |
| X | re-framing | | | X | - the black mirror of data | | |
| Y | navigating | | | Y | - transversal practices | | |
| Z | accelerating | | | Z | - a meditation | | |
 
















 
 
 







































