# Isolation Playground

Are you feeling overwhelmed lately? Do you have difficulties falling asleep? Is Instagram showing you ads for gravity blankets, acupressure mats and meditation apps against your rising levels of stress and anxiety? Did you already try out one of these products? Have you become a creative worker because you didn't want a 9 to 5 job and now you work 24/7? Do you start doubting your life decisions?

This is for you.

This is about refusing feelings of personal shittiness.

You are not an academic impostor.

You have read more than enough.

It's time to share your abilities and to create.

The "Hot take machine" invites you to effortlessly create the title of your next hot paper or presentation. It unleashes your flow of creativity. It activates your neuronal networks towards blissful productivity. The machine has been carefully assembled by revisiting the deep archives of the last decade of transmediale curatorial statements. Just insert your name, your star sign and date of birth, and you will find out immediately which title will get you the most cultural capital. If you want, you can now easily become next year's transmediale keynote speaker on the main stage. You will be the glowing centre of art and theory world's attention.

On isolation playground, creativity is abundant.
