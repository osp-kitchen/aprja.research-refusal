## Recognizing Our Debts - Survey

- Do you feel in debt to the members of your group? If so, how?

- Do you feel in debt to the other eight groups in Research Refusal? If so, how?

- Do you feel you are in debt to transmediale (™)? If so, how?

- Do you feel these people, these groups, and/or these institutions are in debt to you? If so, how?

- How might we re-order our relations of debt to each other and within transmediale?

Anonymous survey to recognize our debts to each other, 21/01/2021. Collected responses can be found at <https://gitlab.com/osp-kitchen/aprja.research-refusal/-/blob/master/Nothing Happening Here/Recognizing_Our_Debts_-_TM_RR_NHH_01.22.21.xlsx>
