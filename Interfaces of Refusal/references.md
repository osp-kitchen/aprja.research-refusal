## Works cited:

- Agre, Philip E. “Toward a Critical Technical Practice: Lessons Learned in Trying to Reform AI.” In _Bridging the Great Divide: Social Science, Technical Systems, and Cooperative Work_. Edited by Geoff Bowker, Les Gasser, Leigh Star, and Turner, Bill. Hillsdale, NJ: Erlbaum, 1997. 
Andersen, Christian Ulrik, and Søren Bro Pold. _The Metainterface_. Cambridge: MIT Press, 2018.

- Egebak, Niels. _Anti-Mimesis_. Arena, 1970.

- Gangadharan, Seeta Peña. “Technologies of Control and Our Right of Refusal.” _TEDxLondon_ (2019).

- Hamid, Sarah T. “Community Defense: Sarah T. Hamid on Abolishing Carceral Technologies.” _Logic Mag_ (2020)

- Katz, Yarden. _Artificial Whiteness: Politics and Ideology in Artificial Intelligence_. Columbia University Press, 2020.

- Lawtoo, Nidesh. "The Powers of Mimesis: Simulation, Encounters, Comic Fascism." _Theory & Event_ 22.3 (2019): 722-746.

- Soon, Winnie and Geoff Cox. _Aesthetic Programming: A Handbook of Software Studies_. London: Open Humanities Press, 2020.

- Vignola, Paolo. "Symptomatology of Collective Knowledge and the Social to Come," _Parallax_ 23.2 (2017): 184-201, DOI: 10.1080/13534645.2017.1299295
