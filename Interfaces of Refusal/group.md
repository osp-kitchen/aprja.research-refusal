# Digital Aesthetics Research Center

DARC is located at Aarhus University and functions as a shared intellectual resource that identifies, analyses, and mediates current topics in digital art and culture. Our aim is to provide new knowledge about the relationship between art, culture and technology. We focus on forming research projects, collaborations and international networks. We publish newspapers and a journal, arrange PhD seminars, internal research seminars, research conferences, and organize public exhibitions and events with digital media artists and researchers from around the world. Besides contributing analytically and theoretically to the field, DARC also engages in practical experiments, and often in collaboration with artists and practitioners. 

URL: https://darc.au.dk
