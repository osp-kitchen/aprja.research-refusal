### References

Aarons, Kieran. *No Selves to Abolish: Afropessimism, Anti-politics & the End of the World*. Ill Will Editions, 2016.

Cummings, A.S. *Brain Magnet: Research Triangle Park and the Idea of the Creative Economy.* Columbia University Press, 2020.

Easterling, Keller. *Extrastatecraft: The Power of Infrastructure Space*. Verso, 2016. 

Halpern, O., R. Mitchell, and B.D. Geoghegan, "The Smartness Mandate: Notes toward a Critique," *Grey Room*, no. 68 (Summer 2017): 106–129.

Nancy, J.-L. (1990). *The inoperative community*. Minneapolis, MN: University of Minnesota Press.

Tuck, E. and K.W. Yang.  ‘[R-Words: Refusing Research’](http://outreach.oregonstate.edu/sites/default/files/r-words.pdf) in D. Paris and M. T. Winn (Eds.) *Humanizing Research: Decolonizing Qualitative Inquiry with youth and Communities*. Thousand Oakes, CA: Sage Publications, 2014. 
