### Governance

GOVERNANCE is at once an collaborative audio-visual noise performance project, and an experimental tactical media collective. Through performative interventions, lectures, and multimedia publications, we view the works of Governance as an exaggeration of the performativity of knowledge production; an investigation of the gestures, affects and embodied dispositions that are culturally legible as ‘knowledge work’ and imbued with a veneer of ‘objectivity."

[https://gvnc.tv/](https://gvnc.tv/)

Rebecca Uliasz is a media artist and PhD Candidate in the department of Computational Media, Arts and Cultures at Duke University and holds an MFA from SUNY Stony Brook. 

Quran Karriem is an experimental musician, media artist and PhD Candidate in the Computational Media, Arts and Cultures program at Duke University. 

Brett Zehner is a Ph.D. candidate at Brown University in Performance Studies and Computational Media and he holds an MFA from UC San Diego.
