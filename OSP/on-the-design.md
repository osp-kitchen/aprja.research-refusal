# DESIGN NOTES

This publication has been designed in a browser using plain text, javascript and git. To include images, HTML and CSS were used. The generated PDF was made ready for print using ghostscript, pdftk and mutools.

The design tool developed for this publication continues on two research tracks. First html2print, a research developed by Open Source Publishing where graphic design for print is produced using web technologies: a browser, HTML, CSS and sometimes javascript. Second the asciiwriter library, developed by Manetta Berends and Gijs de Heij. This experimental Python library draws patterns and non-rectangular layouts using plain text. To allow the library to run in a browser it has been ported to javascript. 

As the tool is web-based the editors can contribute to the layout. To open up the process to more the blockly library was used to support 'visual coding'. The layout is defined through a series of blocks, influencing the text frames, but also their geometry. The material in the publication is retrieved from the git-repository used by the contributors to work on their material. 

Open Source Publishing is a design collective based in Brussels. In a refusal of proprietary software they use only Free / Libre Open Source tools (F/LOSS). This refusal allows to research *what* a graphic design practice can be too. They focus on the relationship between the work produced and the affordances of their tools, and the relation between the makers of tools and how these tools are used. Tools shape practice, practice shapes tools.

F/LOSS shifts the relationship between the user and producer. By explcitly allowing users to study the software, and inviting contributions to the object, the user becomes part producer and responsible for the tool.

OSP is interested in what kind of design can be produced when designers take back control over their toolbox and extend it at will.
