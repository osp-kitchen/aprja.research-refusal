# Contributors 

## Logistical (B)orders - *Refusal to be refused: On modes of resistance to logistical capital in the Global South* (p. 4)

Logistical (B)orders is a multidisciplinary research network invested in examining the differential impacts and contested sites of contemporary extractive capitalism. Working from disparate sites across Asia and the Middle East, they draw upon critical logistics studies, media theory, artistic and activist tactics, Marxism, and decolonial thought to analyze the faultlines in the supply chains of logistical capital where counterpower might arise

- Özgün Eylül İşcen, ICI, Berlin

- Geoffrey Aung, Columbia University, New York

- Sudipto Basu, JNU, New Delhi 

## not working group - *Isolation Playground* (p. 6)

Not working group aims to cultivate hanging out as a mode of research. It adapts to the needs of its members following a simple principle it learned from the gambling industry: When the fun stops, stop. Not working is always open for collaborations and was joined by @afffirmations for this year's transmediale newspaper. New members are always welcome:  isolationplayground@gmail.com.

@afffirmations is your Daily Refill of Positive Affirmations on Instagram. With Quotes for the Heart, @afffirmations inspires a growing number of followers around the globe everyday. Don't forget to check out @afffirmations Instagram for Official Merch.

- Alan Diaz is an architect and theorist based in Mexico City with a research focus on critical philosophy, philosophy of technology and media theory. 

- Lara Scherrieble, Goldsmiths College, is a researcher in the field of pop-culture and resistance and co-founder of the queerfeminist collective and dj-label FAM_. 

- Lukas Stolz is a PhD candidate at the Sociology department of Goldsmiths College interested in post-progressive radical imaginaries in the Anthropocene.

## Governance - *Special_Intellectual_Zone* (p. 8)

[https://gvnc.tv/]

GOVERNANCE is at once an collaborative audio-visual noise performance project, and an experimental tactical media collective. Through performative interventions, lectures, and multimedia publications, we view the works of Governance as an exaggeration of the performativity of knowledge production; an investigation of the gestures, affects and embodied dispositions that are culturally legible as ‘knowledge work’ and imbued with a veneer of ‘objectivity."

- Rebecca Uliasz is a media artist and PhD Candidate in the department of Computational Media, Arts and Cultures at Duke University and holds an MFA from SUNY Stony Brook.

- Quran Karriem is an experimental musician, media artist and PhD Candidate in the Computational Media, Arts and Cultures program at Duke University.

- Brett Zehner is a Ph.D. candidate at Brown University in Performance Studies and Computational Media and he holds an MFA from UC San Diego.

## MELT - *Rituals Against Barriers* (p. 10)

[http://meltionary.com/]

MELT (Loren Britton & Isabel Paehr) are arts-design researchers who engage games, technology and critical pedagogy. Tuning to material-discursive conditions of tech infrastructures, they trouble patterns of agency in socio-technical systems through queer play, unlearning and leaking. Their work crumbles structures, unbounds materials, dissolves technology and makes collectivities. Turning up the heat they read Denise Ferreira da Silva to understand how heat links climate change to colonialism, from there they un-discipline knowledge from transfeminism, computation and chemistry. MELT is shaped by ice, freezing, software, disability justice, moving at trans- crip- kinship- time, Black feminisms, materialisms, de-colonisation, gifs, climate protests, anti-racism and dancing.

## Nothing Happening Here - *Nothing but Debts: Performing the Neo-Institution* (p. 14)

https://nothinghappeninghere.work

Nothing Happening Here is an art-research collective formed from the Speculative Sensation Lab (S-1) at Duke University in 2020. Currently, we are based in Berlin, Athens and Durham, NC. Our work involves nothing, bad debt, refuse, stitching, credit, experiments, machine performance, and instituting otherwise.

- Kelsey Brod is a PhD student in the Computational Media, Arts and Cultures program at Duke University.

- Alexander Strecker is a PhD candidate in the Department of Art, Art History and Visual Studies at Duke University.

- Jordan Sjol is a PhD candidate in the Program in Literature at Duke University.

- Kristen Tapson (PhD) is a Scholar in Residence in the Department of Art, Art History and Visual Studies at Duke University.

- Katia Schwerzmann (PhD) is a research and teaching fellow at the Bauhaus Universität Weimar.

## Bad Bitch Link Up - *How Can I Live? Only in Refusal* (p. 16)

[https://blackfemmetransmediale.wordpress.com]

Our research group consists of four anti-disciplinary artists and scholars navigating refusal across mediums and fields of study both within and without the academy. Together, we aim to highlight enduring lineages of refusal while attending to contemporary moves towards redress and reparative justice. We’re inspired by the sustenance of the quotidian, the exponential facts of Blackness, and the ancient futures of non-colonialism, which all undergird our refusals and calls for UBI & reparations. All members are graduates from UCLA:

- alea adigweme is a multidisciplinary Igbo-Vincentian cultural worker based in Tovaangar, the unceded Tongva land commonly called Los Angeles. 

- Catherine Feliz is an interdisciplinary artist born & raised in Lenape territory (NYC) to parents from Kiskeya Ayiti (Dominican Republic). 

- Kearra Amaya Gopee is a multidisciplinary visual artist from Carapichaima, Trinidad & Tobago, based in Los Angeles, CA. 

- A.E. Stevenson is a Ph.D. candidate in Cinema & Media Studies. 

## Bucles - *Embrace No Certainties: or, How to Refuse Capitalist Realism* (p. 18)

[https://Bucles.info]

Bucles is an in-process, independent and transdisciplinary research group working in the intersection of media studies, media theories, and critical theory, with an open and ongoing interest in philosophy of technology, media aesthetics, and the history of science and cybernetics. While originally formed in 2017 as an informal unit of inquiry, dialogue, and thought in Santiago, Chile, our current geographical separation has pushed our actions to resort to telematic conversations, literature discussions, and paper assessments – all of which in turn informs our own individual processes.

- Diego Gómez-Venegas, PhD researcher at Humboldt-Universität zu Berlin (Media Sciences)

- Joaquín Zerené, PhD researcher Universidad Austral de Chile (Human Sciences)

- Dusan Cotoras, Research Assistant at "Ciencia Chile Contemporaneo" group (STS)

## DARC (Digital Aesthetics Research Center) - *Interfaces of Refusal: Critical Technical (Research) Practice* (p. 20)

[https://darc.au.dk]

DARC is located at Aarhus University and functions as a shared intellectual resource that identifies, analyses, and mediates current topics in digital art and culture. Our aim is to provide new knowledge about the relationship between art, culture and technology. We focus on forming research projects, collaborations and international networks. We publish newspapers and a journal, arrange PhD seminars, internal research seminars, research conferences, and organize public exhibitions and events with digital media artists and researchers from around the world. Besides contributing analytically and theoretically to the field, DARC also engages in practical experiments, and often in collaboration with artists and practitioners.

- Malthe Stavning Erslev, PhD Researcher

- Gabriel Pereira, PhD Researcher

- Emanuele Andreoli, PhD Researcher

- Søren Pold, Associate Professor

- Winnie Soon, Associate Professor

- Magda Tyżlik-Carver, Associate Professor

- Christian Ulrik Andersen, Associate Professor

## CSNI (Centre for the Study of the Networked Image) - *A Partial Lexicon of Delinking: a pluriversal exercise of fragments and multiple positions* (p. 22)

[https://www.centreforthestudyof.net/]

CSNI (Centre for the Study of the Networked Image) is a research centre based at London South Bank University. It brings together researchers from cultural studies, contemporary art and media practice, and software studies, who seek knowledge and understanding of how network culture transforms the production and circulation of images. Our aim is to broaden the discussion of the networked image to address planetary scale computation and wider ecologies including the non-human. 

- Marloes De Valk, PhD Researcher

- Rosie Hermon, PhD Researcher


