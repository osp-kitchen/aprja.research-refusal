# Bucles 

Bucles is an in-process, independent and transdisciplinary research group working in the intersection of media studies, media theories, and critical theory, with an open and ongoing interest in philosophy of technology, media aesthetics, and the history of science and cybernetics. While originally formed in 2017 as an informal unit of inquiry, dialogue, and thought in Santiago, Chile, our current geographical separation has pushed our actions to resort to telematic conversations, literature discussions, and paper assessments – all of which in turn informs our own individual processes.

bucles.info
