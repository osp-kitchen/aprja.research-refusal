# REFERENCES

- Aarons, Kieran. *No Selves to Abolish: Afropessimism, Anti-politics & the End of the World*. Ill Will Editions, 2016.

- Abolition & Disability Justice Collective. https://abolitionanddisabilityjustice.com.

- Agre, Philip E. "Toward a Critical Technical Practice: Lessons Learned in Trying to Reform AI." In *Bridging the Great Divide: Social Science, Technical Systems, and Cooperative Work*. Edited by Geoff Bowker, Les Gasser, Susan Leigh Star, and Bill Turner. Erlbaum, 1997.

- Andersen, Christian Ulrik, and Søren Bro Pold. *The Metainterface*. The MIT Press, 2018.

- Athanasiou, Athena. "Performing the Institution 'as if it Were Possible.'" *Former West: Art and the Contemporary After 1989*. Edited by Maria Hlavajova and Simon Sheikh. BAK/The MIT Press, 2016, 679-92.

- Berardi, Franco. *The Soul at Work*. The MIT Press, 2009.

- Butler, Judith. *Notes Toward a Performative Theory of Assembly*. Harvard University Press, 2015.

- Campbell, F. Kumari. "Inciting Legal Fictions: Disability’s date with ontology and the ableist body of the law." *Griffith Law Review* 10 (2001): 42–62.

- Campt, Tina M. *Listening to Images*. Duke University Press, 2017.

- *Chocolate Babies*. Directed by Stephen Winter (1996). 

- Chua, C. 2020. "Post-Imperial Oceanics | Charmaine Chua." https://youtu.be/2Xz0PXtNUpE (2020).

- Costanza-Chock, Sasha. *Design Justice: Community-Led Practices to Build the Worlds We Need*. The MIT Press, 2020.

- Critical Design Lab, https://cripritual.com/, 2020. 

- Cummings, Alex Sayf. *Brain Magnet: Research Triangle Park and the Idea of the Creative Economy*. Columbia University Press, 2020.

- Danyluk, Martin. "Capital's Logistical Fix: Accumulation, globalization, and the survival of capitalism." *Environment and Planning D: Society and Space*, 36.4 (2017): 630-647.

- Deleuze, Gilles and Félix Guattari. *Anti-Oedipus: Capitalism and Schizophrenia*. University of Minnesota Press, 1983.

- Disability and Intersectionality Summit. "10 Places to Start." https://www.disabilityintersectionalitysummit.com/places-to-start.

- Easterling, Keller. *Extrastatecraft: The Power of Infrastructure Space*. Verso, 2016.

- Egebak, Niels. *Anti-Mimesis*. Arena, 1970.

- Ernst, Wolfgang. *Digital Memory and The Archive*. University of Minnesota Press, 2013.

- Fisher, Mark. *Capitalist Realism: Is There No Alternative?*. Zero Books, 2009.

- Fisher, Mark. "No Romance Without Finance." In *K-punk: The Collected and Unpublished Writings of Mark Fisher*. Repeater, 2018.

- Galloway, Alexander R., and Eugene Thacker. *The Exploit: A Theory of Networks*. University of Minnesota Press, 2007.

- Gangadharan, Seeta Peña. "Technologies of Control and Our Right of Refusal." TEDxLondon (2019).

- Garland‐Thomson, Rosemarie. "Misfits: A feminist materialist disability concept." *Hypatia* 26.3 (2011): 591-609.

- Gumbs, Alexis Pauline. *Undrowned: Black Feminist Lessons from Marine Mammals*. Emergent Strategy Series 2, AK Press, 2020.

- Halberstam, Jack. *The Queer Art of Failure*. Duke University Press, 2011.

- Halpern, Orit, Robert Mitchell, and Bernard Dionysius Geoghegan, "The Smartness Mandate: Notes toward a Critique." *Grey Room* 68 (Summer 2017): 106–129.

- Hamid, Sarah T. "Community Defense: Sarah T. Hamid on Abolishing Carceral Technologies." *Logic Mag* (2020).

- Hamraie, Aimi, and Kelly Fritsch. "Crip Technoscience Manifesto." *Catalyst: Feminism, Theory, Technoscience* 5.1 (2019): 1-33.

- Haraway, Donna J. "Situated Knowledges: The Science Question in Feminism and the Privilege of Partial Perspective." *Feminist Studies* 14.3 (1988): 575-99.

- Harney, Stefano, and Fred Moten. *The Undercommons: Fugitive Planning and Black Study*. Minor Compositions, 2013.

- Harney, Stefano, and Fred Moten. "Debt and Study." *e-flux* 14 (2010): 1–5.

- Hartman, Saidiya. "The Anarchy of Colored Girls Assembled in a Riotous Manner." *The South Atlantic Quarterly*, July (2018).

- Heisenberg, Werner. *Physics and Philosophy*. Harper & Brothers, 1958.

- Jackson, Zakiyyah Iman. "'Theorizing in a Void': Sublimity, Matter, and Physics in Black Feminist Poetics." *South Atlantic Quarterly* 117.3 (2018)): 617–648.

- Khalili, Laleh. "Behind the Beirut Explosion Lies the Lawless World of International Shipping." *The Guardian*, August 8 (2020). https://www.theguardian.com/commentisfree/2020/aug/08/beirut-explosion-lawless-world-international-shipping-.

- Katz, Yarden. *Artificial Whiteness: Politics and Ideology in Artificial Intelligence*. Columbia University Press, 2020.

- Lawtoo, Nidesh. "The Powers of Mimesis: Simulation, Encounters, Comic Fascism." *Theory & Event* 22.3 (2019): 722-746.

- Lazzarato, Maurizio. *Signs and Machines*. The MIT Press, 2014.

- Marx, Karl. *Grundrisse*. Penguin Books, 1973.

- Mezzadra, Sandro, and Brett Neilson. *Border as Method, or, The Multiplication of Labor*. Duke University Press, 2013.

- Mignolo, Walter. "Foreword: On Pluriversality and Multipolarity" & "On Pluriversality and Multipolar World Order." In *Constructing the Pluriverse*. Edited by Bernd Reiter. Duke University Press, 2018.

- Mingus, Mia. "Access intimacy, interdependence, and disability justice." *Leaving Evidence* (2017). https://leavingevidence.wordpress.com/2017/04/12/access-intimacyinterdependence-and-disability-justice/.

- Nancy, Jean-Luc. *The Inoperative Community*. University of Minnesota Press, 1990.

- Newman, Bryan. "A Bitter Harvest: Farmer suicide and the unforeseen social, environmental and economic impacts of the Green Revolution in Punjab, India." *Institute for Food and Development Policy, Development Report No. 15* (2007): 1-34.

- Rosner, Daniela K. *Critical Fabulations: Reworking the Methods and Margins of Design*. The MIT Press, 2018.

- Samuels, Ellen. "Six Ways of Looking at Crip Time." *Disability Studies Quarterly* 37.3 (2017). https://dsq-sds.org/article/view/5824/4684.

- Sandhu, Amandeep. "Left, Khaps, Gender, Caste: The solidarities propping up the farmers’ protest." *The Caravan* 13 January (2021). https://caravanmagazine.in/agriculture/left-punjab-haryana-caste-gender-solidarities-farmers-protest.

- Sen, Abhijit, and Jayati Ghosh. "Indian Agriculture after Liberalisation." *Bangladesh Development Studies*  XXXX, A (2017): 53-71.

- Shukaitis, Stevphen. "Learning Not to Labor." *Rethinking Marxism: A Journal of Economics, Culture & Society* 26.2 (2014): 193-205. 

- Simpkins, Reese. "Trans* feminist Intersections." *Transgender Studies Quarterly* 3.1-2 (2016): 228-234. https://doi.org/10.1215/23289252-3334427.

- Simondon, Gilbert. *Individuation in the Light of Notions of Form and Information*. University of Minnesota Press, 2020.

- *Skin, Tooth, and Bone: The Basis of Movement is Our People*. Sins Invalid. 1st ed., digital ed., 2016. 

- Sloterdijk, Peter. *Critique of Cynical Reason*. University of Minnesota Press, 1987.

- Smilges, Jonathan. "Bad Listeners." *Peitho* 23.1, Fall (2020). https://cfshrc.org/article/bad-listeners/.

- Soon, Winnie and Geoff Cox. *Aesthetic Programming: A Handbook of Software Studies*. Open Humanities Press, 2020.

- Tronti, Mario. "The Strategy of the Refusal." *Operai e Capitale*. Einaudi, 1966, 234-252.

- Tsing, Anna Lowenhaupt . "On Nonscalability: The Living World Is Not Amenable to Precision-Nested Scales." *Common Knowledge* 18.3 (2012): 505-524.

- Tuck, Eve, and K. Wayne Yang. "R-Words: Refusing Research." *Humanizing Research: Decolonizing Qualitative Inquiry With Youth and Communities*, edited by Django Paris and Maisha T. Winn. Sage (2014): 223-47.

- Vignola, Paolo. "Symptomatology of Collective Knowledge and the Social to Come." *Parallax* 23.2 (2017): 184-201.

- Walker, Nick. "Neuroqueer: An Introduction." https://neurocosmopolitanism.com/neuroqueer-an-introduction/.

- Wynter, Sylvia, "Beyond the Word of Man: Glissant and the New Discourse of the Antilles." *World Literature Today* 63.4, Edouard Glissant Issue (Autumn, 1989): 637-648.

- Wynter, Sylvia, "Ethno or Socio Poetics." *Alcheringa/Ethnopoetics* 2.2 (1976): 78-94.

- Ziadah, Rafeef. "Transport Infrastructure & Logistics in the Making of Dubai Inc." *International Journal of Urban and Regional Research* (2018): 182-197.

- Žižek, Slavoj. *The Sublime Object of Ideology*. Verso, 1989.

