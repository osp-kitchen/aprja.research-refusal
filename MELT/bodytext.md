# Rituals Against Barriers

Working with refusal and the Crip Technoscience Manifesto by Kelly Fritsch and Aimi Hamraie, we have developed a project called *Rituals Against Barriers*. Below, we answer some questions about this emerging work.

## What is *Rituals against Barriers*?

*Rituals against Barriers* is built on the multiple practices of survival in the face of oppression which take the form of ritual practice. Rituals in our poster on the next page offer space for perceiving, questioning, dissolving and transforming barriers. Rituals are a Black, trans* and crip transformative survival means. Rituals can create a moment in which oppressive dynamics are not the main focus of attention, or the ritual itself acts as a resistant practice to deal with normative conditions.

*Rituals against Barriers* are a research, design and sharing project that collectivizes practices of refusing barriers.

## What is a barrier? What is normative space?

A barrier in this work is a structural condition or unreflected habit that prevents people from entering or being in a space. These can include so called 'obvious' barriers like stairs, but also can include less 'obvious' barriers like smells. 

In normative space, otherness is measured against a presumed "corporeal standard" (Campbell 2001) of cis, able-bodiedness, heterosexuality and whiteness. This paradigm appears as though there are no alternatives; this is what we work against. Working with trans* and crip knowing-making from the intersectional framework of Disability Justice (Sins Invalid 2016), we connect refusal to non-normative access practices to refuse assimilation into normative orders.

## What are rituals?

Rituals exist throughout all cultures: from cripritual.com, we read "Disabled, crip, d/Deaf, Mad, and Sick people face a lot of barriers and stigma. One way that we deal with these barriers is through rituals. Rituals can be things that we do to create accessibility, mark important moments, or to be in community with others who have similar experiences." This resonates with us as we engage ritual as a method, and practice it nearby the work of Tina Campt. In Listening to Images, she describes rituals as "...practices that are pervasive and ever-present yet ocluded by their seeming absence or erasure in representation, routine or internalization." and continues, these are "practice(s) honed by the disposessed in the struggle to create possibility within the constraints of everyday life [...] [the] quiet and the quotidian are mobilized as everyday practices of refusal." (p. 4)

Rituals can invite a stepping away from whatever normativities and can allow entry into practices such as: 
refusing to ignore a feeling, refusing to not listen to your body because doing so would make apparent the ableism of any space, refusing to speed up even if that is the normalised tempo. Rituals are often a pathologized aspect of a lived disability experience. One example of this is stimming, the repetition of movements or sounds that one finds calming or joyful – rituals, rituals, rituals. Rituals can be moments of joy, of refusal, of collective practice, of uncomfort and of unlearning. 

## What is access?

Access is a process of reducing a barrier which allows for people to enter a space. But what are the terms upon which people can do so? Sometimes, people are required to conform with normative standards as a prerequisite for gaining access. This makes access a frictioned term, meaning that depending on how access is understood and practiced, it can bring joy or harm to marginalized people.

Access, like care, is something that cannot be determined in advance and is an ongoing process. Often, access in (some) laws is defined as including a ramp, gender neutral bathrooms, closed captions or sign language interpretation. We work with processes of access intimacies (Mingus), which challenge us to understand the contextual specificity and ongoing instability of any accessibilities. Access is never self-evident or value neutral.

## How does this relate to refusal?

Crip Technoscience describes "practices of critique, alteration, and reinvention of our material-discursive world." (p. 2), it is also a "field of knowing" (ibid.) With the term crip, Hamraie and Fritsch point to "the non-compliant, anti-assimilationist position that disability is a desirable part of the world". (ibid.) 

Refusal, for us, starts from a political understanding that disability is an important and not to be 'corrected' experience. Disability is something we want to be part of the world therefore we refuse any 'access' mechanisms that impose normative standards: this refusal divests from solutionism.

## Why wedges and what do they do in this work?

We think from ritual, because a ritual often says something about how people work within and around given conditions. The concept of "misfitting" (Garland-Thompson 2011) describes how deviances from a normative standard can become a source of knowing-making. Our knowing-making method is tied to misfitting as our rituals provoke moments of questioning who and what fits. On the next two pages you will find that our rituals are accompanied by images of wedges. A wedge is a triangular shape or cone that has a thick tapering to a thin edge. It can secure or separate objects such a door and a door frame, or one piece of wood into two or more. Wedges that hold some doors open in varying angles and shut others are interesting for barrier reducing work. Not every wedge can create access through every door. The wedge is a difference making device, and a hacking device in two ways: it rough cuts materials, and it makes possible to gain unauthorized access into closed systems. Thus the wedge picks up access as both attack and contact.

By driving a wedge into systemic practices that ignore difference, our rituals: attend to differences, make soft hard systems (and structures), render things as processes (through repetition) rather than as static, make immediate change and amplify changes that are already ongoing. 

# Crip Technoscience Manifesto, Aimi Hamraie & Kelly Fritsch, 2019
In their *Crip Technoscience Manifesto*, Aimi Hamraie and Kelly Fritsch describe crip technoscience as "practices of critique, alteration, and reinvention of our material-discursive world." (p. 2) as well as a "field of knowing" (ibid.) With the term crip, Hamraie and Fritsch point to "the non-compliant, anti-assimilationist position that disability is a desirable part of the world". (ibid.) Crip technoscience centers the work of disabled people as knowers and makers, is committed to access as friction and builds on interdependence and disability justice.

