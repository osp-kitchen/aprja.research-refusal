## References

- Abolition & Disability Justice Collective. [https://abolitionanddisabilityjustice.com](https://abolitionanddisabilityjustice.com) (last accessed 05.03.21)

- Campbell, F. Kumari. 2001. "Inciting legal fictions: Disability’s date with ontology and the ableist body of the law". Griffith Law Review. 10, 42–62

- Campt, Tina M. Listening to images. Duke University Press, 2017.

- Costanza-Chock, Sasha. Design Justice: Community-Led Practices to Build the Worlds We Need. MIT Press, 2020.

- Critical Design Lab, [cripritual.com](https://cripritual.com/), 2020 (last accessed 05.03.21)

- Disability and Intersectionality Summit. "10 Places to Start". [https://www.disabilityintersectionalitysummit.com/places-to-start](https://www.disabilityintersectionalitysummit.com/places-to-start).

- Garland‐Thomson, Rosemarie. "Misfits: A feminist materialist disability concept." Hypatia 26.3 (2011): 591-609.

- Gumbs, Alexis Pauline. Undrowned. Black Feminist Lessons from Marine Mammals. Emergent Strategy Series No. 2, AK Press (2020): 109

- Hamraie, Aimi, and Kelly Fritsch. "Crip technoscience manifesto." Catalyst: Feminism, Theory, Technoscience 5.1 (2019): 1-33.

- Mingus, Mia. (2017). Access intimacy, interdependence, and disability justice. Leaving Evidence. [https://leavingevidence.wordpress.com/2017/04/12/access-intimacyinterdependence-and-disability-justice/](https://leavingevidence.wordpress.com/2017/04/12/access-intimacyinterdependence-and-disability-justice/) (last accessed 05.03.21)

- Neuroqueer, an Introduction. [https://neurocosmopolitanism.com/neuroqueer-an-introduction/](https://neurocosmopolitanism.com/neuroqueer-an-introduction/)

- Rosner, Daniela K. Critical Fabulations: Reworking the Methods and Margins of Design. MIT Press, 2018.

- Samuels, Ellen. "Six Ways of Looking at Crip Time." Disability Studies Quarterly Vol 37, No 3, 2017. [https://dsq-sds.org/article/view/5824/4684](https://dsq-sds.org/article/view/5824/4684)

- Simpkins, Reese. "Trans* feminist Intersections." Transgender Studies Quarterly 3.1-2 (2016), 228-234. [https://doi.org/10.1215/23289252-3334427](https://doi.org/10.1215/23289252-3334427)

- Sins Invalid. Skin, Tooth, and Bone: The Basis of Movement is Our People. 1st ed., digital ed., 2016. 

- Smilges, Johnathan. "Bad Listeners." Peitho Volume 23 Issue 1 Fall, 2020. [https://cfshrc.org/article/bad-listeners/](https://cfshrc.org/article/bad-listeners/)

- Wynter, Sylvia, cited in McKittrick, Katherine: Wynter, Sylvia, “Beyond the Word of Man,” 638–39. See also: Sylvia Wynter, “Ethno or Socio Poetics,” 87.
